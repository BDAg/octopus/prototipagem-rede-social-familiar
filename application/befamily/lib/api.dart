import 'package:http/http.dart' as http;
import 'dart:convert';

class Api {

  // String url = 'http://10.0.2.2:4000/';
  // String url = 'https://api-befamily.herokuapp.com/';
  String url = 'http://35.231.220.66:4000/';

  Future<http.Response> login(String email, String password) async {
    Map<String, dynamic> bodyPost = {'emailOrUsername': email, 'senha': password};

    http.Response response =
        await http.post(url + 'user/login', body: bodyPost);
    return response;
  }

  Future<http.Response> verifyToken(String token) async {
    Map<String, dynamic> bodyPost = {'token': token};

    http.Response response =
        await http.post(url + 'user/verify-token', body: bodyPost);
    return response;
  }

  Future<http.Response> sendMail(String email) async {

    Map<String, dynamic> bodyPost = { 'email': email };

    http.Response response = await http.post(url + 'user/forgot-password', body: bodyPost);

    return response;

  }

  Future<http.Response> verifyCode(String email, String code) async {

    Map<String, dynamic> bodyPost = { 'email': email, 'code': code };

    http.Response response = await http.post(url + 'user/verify-code', body: bodyPost);

    return response;

  }

  Future<http.Response> changePasswordCode(String email, String code, String newPassword) async {

    Map<String, dynamic> bodyPost = { 'email': email, 'code': code, 'newPassword': newPassword };

    http.Response response = await http.post(url + 'user/change-password-code', body: bodyPost);

    return response;

  }

  Future<http.Response> changePassword(String token, String oldPassword, String newPassword) async {

    Map<String, dynamic> bodyPost = { 'token': token, 'oldPassword': oldPassword, 'newPassword': newPassword };

    http.Response response = await http.post(url + 'user/change-password', body: bodyPost);
    return response;

  }

  Future<http.Response> createUser(Map<String, dynamic> data) async {

    http.Response response = await http.post(url + 'user/create-user', body: data);

    return response;

  }

  Future<http.Response> verifyUsername(String username) async {

    http.Response response = await http.post(url + 'user/verify-username', body: { 'username': username });
    return response;

  }

  Future<http.Response> verifyEmail(String email) async {

    http.Response response = await http.post(url + 'user/verify-email', body: { 'email': email });
    return response;

  }

  Future<http.Response> verifyFeed(int timestamp, String token, List families) async {

    http.Response response = await http.post(
      url + 'midia/verify-feed',
      body: { 
        'timestamp': timestamp.toString(),
        'token': token,
        'idFamilia': json.encode(families).toString()
      },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;

  }

  Future<http.Response> cities(String city) async {
    http.Response response = await http.post(url + 'util/cities', body: { 'cidade': city });
    return response;
  }

  Future<http.Response> upload(String tokenUser, String token, String nomeImg, String image) async {
    print(image);
    http.Response response = await http.post(
      url + 'midia/upload',
      body: { 'token': token, 'nomeImg': nomeImg, 'image': image },
      headers: { 'Authorization': 'bearer $tokenUser' }
    );
    print(response.body);
    return response;
  }

  Future<http.Response> updateFotoUser(String token, String path) async {
    http.Response response = await http.post(
      url + 'user/update-foto',
      body: { 'token': token, 'path': path },
      headers: { 'Authorization': 'bearer $token' }
    );
    return response;
  }

  Future<http.Response> createFamily(Map<String, dynamic> data, String token) async {

    http.Response response = await http.post(
      url + 'family/create-family',
      body: data,
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;

  }

  Future<http.Response> updateFotoFamily(String token, String idFamily, String path) async {
    http.Response response = await http.post(
      url + 'family/update-foto',
      body: { 'token': token, 'path': path, 'idFamily': idFamily },
      headers: { 'Authorization': 'bearer $token' }
    );
    return response;
  }

  Future<http.Response> searchFamiliesUser(String tokenUser) async {
    http.Response response = await http.post(
      url + 'family/find-family',
      body: { 'token': tokenUser },
      headers: { 'Authorization': 'bearer $tokenUser' }
    );
    return response;
  }

  Future<http.Response> joinFamily(String tokenUser, String idFamily) async {
    http.Response response = await http.post(
      url + 'family/join-family',
      body: { 'token': tokenUser, 'idFamily': idFamily },
      headers: { 'Authorization': 'bearer $tokenUser' }
    );
    return response;
  }

  Future<http.Response> usersInfo(String tokenUser, List users) async {

    http.Response response = await http.post(
      url + 'user/users-info',
      body: { 'token': tokenUser, 'users': json.encode(users).toString() },
      headers: { 'Authorization': 'bearer $tokenUser' }
    );

    return response;
  }

  Future<http.Response> publicar(String tokenUser, int tipoPublicacao, String legenda, List families, Map info) async {

    Map dados = { 
      'token': tokenUser,
      'tipo': tipoPublicacao.toString(),
      'legenda': legenda,
      'idFamilia': json.encode(families).toString(),
      'path': info['path'],
      'height': info['height'].toString(),
      'width': info['width'].toString()
    };

    http.Response response = await http.post(
      url + 'midia/post-midia',
      body: dados,
      headers: { 'Authorization': 'bearer $tokenUser' }
    );

    return response;
  }

  Future<http.Response> listMideas(String tokenUser, List families) async {

    http.Response response = await http.post(
      url + 'midia/list',
      body: { 'token': tokenUser, 'idFamilia': json.encode(families).toString() },
      headers: { 'Authorization': 'bearer $tokenUser' }
    );
    return response;
  }

  Future<http.Response> listMideasUser(String token) async {

    http.Response response = await http.post(
      url + 'midia/list-user',
      body: { 'token': token },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

  Future<http.Response> getMideasFamily(String token, String idFamily) async {
    http.Response response = await http.post(
      url + 'midia/list-family',
      body: { 'idFamily': idFamily },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

  Future<http.Response> like(String token, String idMidia) async {
    http.Response response = await http.post(
      url + 'midia/like',
      body: { 'idMidia': idMidia, 'token': token },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

  Future<http.Response> deleteMidea(String token, String idMidia) async {
    http.Response response = await http.post(
      url + 'midia/delete-midia',
      body: { 'idMidia': idMidia, 'token': token },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

  Future<http.Response> listCommentaries(String token, String idMidia) async {
    http.Response response = await http.post(
      url + 'comment/list-commentaries',
      body: { 'idMidia': idMidia },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

  Future<http.Response> postCommentary(String token, String idMidia, String comentario) async {
    http.Response response = await http.post(
      url + 'comment/make-commentary',
      body: { 'idMidia': idMidia, 'comentario': comentario, 'token':token },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

  Future<http.Response> deleteCommentary(String token, String idComentario) async {
    http.Response response = await http.post(
      url + 'comment/delete-commentary',
      body: { 'idComentario': idComentario, 'token': token },
      headers: { 'Authorization': 'bearer $token' }
    );

    return response;
  }

}
