import 'package:befamily/pages/family/MembrosFamilia.dart';
import 'package:befamily/pages/family/criarFamilia.dart';
import 'package:befamily/pages/family/joinFamilia.dart';
import 'package:befamily/pages/family/perfilFamilia.dart';
import 'package:befamily/pages/home.dart';
import 'package:befamily/pages/splash/splashScreen.dart';
import 'package:befamily/pages/user/cadastroPessoal.dart';
import 'package:befamily/pages/user/cadastroUsuario.dart';
import 'package:befamily/pages/user/emocoes.dart';
import 'package:befamily/pages/user/esqueciSenha.dart';
import 'package:befamily/pages/user/familyList.dart';
import 'package:befamily/pages/user/login.dart';
import 'package:befamily/pages/user/redefinirSenhaEmail.dart';
import 'package:flutter/material.dart';

final routes = {
  '/splash': (BuildContext context) => SplashScreen(),
  '/login': (BuildContext context) => Login(),
  '/esqueci-senha': (BuildContext context) => EsqueciSenha(),
  '/cadastro': (BuildContext context) => CadastroUsuario(),
  '/redefinir-senha-email': (BuildContext context) => RedefinirSenhaEmail(),
  '/cadastro-pessoa': (BuildContext context) => CadastroPessoal(),
  '/criar-familia': (BuildContext context) => CriarFamilia(),
  '/home': (BuildContext context) => Home(),
  '/join-familia': (BuildContext context) => JoinFamilia(),
  '/perfil-familia': (BuildContext context) => PerfilFamilia(),
  '/membros-familia': (BuildContext context) => MembrosFamilia(),
  '/emocoes': (BuildContext context) => Emocoes(),
  '/family-list': (BuildContext context) => FamilyList()
};
