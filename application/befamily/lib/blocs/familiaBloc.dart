import 'package:shared_preferences/shared_preferences.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:async';

import 'package:befamily/api.dart';
import 'package:befamily/models/familia.dart';

class FamiliaBloc implements BlocBase {

  Api api;

  FamiliaModel familyData;

  List _families;

  List get families => _families;

  FamiliaBloc() {
    api = Api();
    // SharedPreferences.getInstance().then(
    //   (prefs) {
    //     if (prefs.getKeys().contains('families')) {
    //       _families = json.decode(prefs.getString('families'));
    //     }
    //   }
    // );
  }

  @override
  void dispose() { 
  }

  setFamilies(List dados) async {
    _families = dados;
    // await _save();
  }

  Future updateFotoPerfil(String tokenUser, String path, String idFamily, VoidCallback _success, VoidCallback _fail) async {
    
    final response = await api.updateFotoFamily(tokenUser, idFamily, path);

    if (response.statusCode == 200) {
      return _success();
    } else {
      return _fail();
    }
  }

  Future joinFamily(String tokenUser, String idFamily, VoidCallback _success, VoidCallback _fail) async {
    final response = await api.joinFamily(tokenUser, idFamily);
    
    if (response.statusCode == 200) {
      _success();
    } else {
      _fail();
    }

  }

  Future usersInfo(String tokenUser, List users) async {

    final response = await api.usersInfo(tokenUser, users);

    List infoUsers;

    if (response.statusCode == 200) {
      infoUsers = json.decode(response.body)['response']['data'];
    }

    return infoUsers;

  }

  void logout() async {
    this._families = [];
    // await _save();
  }

  // _save() async {
  //   await SharedPreferences.getInstance().then(
  //     (prefs) {
  //       prefs.setString("families", json.encode(_families));
  //     }
  //   );
  // }


}