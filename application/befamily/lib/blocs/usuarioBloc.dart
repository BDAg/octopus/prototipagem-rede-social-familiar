import 'dart:async';
import 'dart:convert';

import 'package:befamily/api.dart';
import 'package:befamily/models/usuario.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UsuarioBloc implements BlocBase {
  Api api;

  bool _logged = false;
  bool _firstTime = false;

  String _token;
  String _code;
  String _emailRecovery;

  int timestampFeed;

  UsuarioModel _userData;
  UsuarioModel _userDataRegister;

  final _userController = StreamController.broadcast();
  final _momentsController = StreamController.broadcast();
  final _updateFeedController = StreamController.broadcast();

  final pageStream = StreamController<int>.broadcast();

  Stream get pageS => pageStream.stream;
  
  Stream get outLogged => _userController.stream;
  Stream get moments => _momentsController.stream;
  Stream get updateFeed => _updateFeedController.stream;

  UsuarioModel get userData => _userData;
  UsuarioModel get userDataRegister => _userDataRegister;

  bool get logged => _logged;
  bool get firstTime => _firstTime;

  String get token => _token;

  UsuarioBloc() {
    api = Api();

    SharedPreferences.getInstance().then((prefs) {
      if (prefs.getKeys().contains('user')) {
        _userData = UsuarioModel.fromJson(json.decode(prefs.getString('user')));
        _logged = json.decode(prefs.getString('logged'));
        _token = json.decode(prefs.getString('token'));
        _emailRecovery = prefs.getString('emailRecovery');

        verifyToken();
      }

      var now = new DateTime.now();
      var dataComTimeStamp = now.toIso8601String();
      var dataTimeStamp = DateTime.parse(now.toIso8601String());

      if (prefs.getKeys().contains('date')) {
        var difference =
            dataTimeStamp.difference(DateTime.parse(prefs.getString('date')));
        if (difference.inHours <= 24) {
          //dataTimeStamp 2019-05-19 15:01:43.804883
          //difference 0:57:28.112796
          //2019-05-19 14:04:15.692087
          //guardar invez da ultima hora que entrou a diferença de agora pra
          //ultima vez, se a diferença for mais que 24 horas muda o fistTime para true
          _firstTime = false;
        } else {
          _firstTime = true;
          prefs.setString('date', dataComTimeStamp);
        }
      } else {
        prefs.setString('date', dataComTimeStamp);
        _firstTime = true;
      }
    });
  }

  setIndex(int index) {
    pageStream.add(index);
  }

  setCode(String code) {
    _code = code;
  }

  setMoments(int qtd) {
    _momentsController.add(qtd);
  }

  setTimestamp(int time) {
    this.timestampFeed = time;
  }

  setUserDataRegister(
      UsuarioModel user, VoidCallback _success, VoidCallback _fail) {
    _userDataRegister = user;
    api.verifyEmail(user.email).then((response) {
      if (response.statusCode == 200) {
        _success();
      } else {
        _fail();
      }
    }).catchError((err) {
      _fail();
    });
  }

  setEmailRecovery(String email) async {
    _emailRecovery = email;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString("emailRecovery", email);
    });
  }

  createUser() async {
    final auxUser = _userDataRegister.toJsonApi();

    if (auxUser['fotoPerfil'] == null) {
      auxUser['fotoPerfil'] = '';
    }

    if (auxUser['religiao'] == null) {
      auxUser['religiao'] = '';
    }

    final response = await api.createUser(auxUser);
    String dados;

    if (response.statusCode == 201) {
      dados = json.decode(response.body)['response']['token'];
    }

    return dados;
  }

  login(String email, String password, Function onSuccess(List families),
      VoidCallback onFail) async {
    api.login(email, password).then((data) {
      if (data.statusCode == 201) {
        Map<String, dynamic> dados = json.decode(data.body)['response'];
        _userData = UsuarioModel.fromJson(dados['data']);
        _token = dados['token'];
        _logged = true;
        _userController.sink.add(_logged);
        _save();
        onSuccess(dados['data']['families']);
      } else {
        _save();

        onFail();
      }
    });
  }

  void logout(VoidCallback _success) async {
    _success();
    _userData = UsuarioModel();
    _logged = false;
    _token = '';
    _save();
  }

  void verifyToken() async {
    if (_token.isNotEmpty) {
      api.verifyToken(_token).then((response) {
        if (response.statusCode != 200) {
          _userData = UsuarioModel();
          _logged = false;
          _token = '';
          _save();
        }
      });
    }
  }

  void verifyFeed(List families) async {
    bool data = false;
    final response = await api.verifyFeed(this.timestampFeed, this.token, families);

    if (response.statusCode == 200) {
      data = json.decode(response.body)['response']['update'];
    }

    if (data == true) {
      _updateFeedController.add(data);
    }

    print('chamou');

    if (this.logged) {
      Future.delayed(Duration(seconds: 6), () {
        this.verifyFeed(families);
      });
    }

  }

  void sendMail(String email, VoidCallback _success, VoidCallback _fail) {
    api.sendMail(email).then((response) {
      print(response.body);
      if (response.statusCode == 200) {
        _success();
        setEmailRecovery(email);
      } else {
        _fail();
      }
    });
  }

  void publicar(String tokenUser, int tipoPublicacao, String legenda, List families, Map dados, VoidCallback _success, VoidCallback _fail) {
    api.publicar(tokenUser, tipoPublicacao, legenda, families, dados).then((response) {
      if (response.statusCode == 201) {
        _success();
      } else {
        _fail();
      }
    });
  }

  getMideas(String token, List families) async {
    List data;

    final response = await api.listMideas(token, families);

    if (response.statusCode == 200) {
      data = json.decode(response.body)['response']['data'];
    }

    return data;
  }

  like(String token, String idMidia) async {
    Map data;

    final response = await api.like(token, idMidia);

    if (response.statusCode == 200) {
      data = json.decode(response.body);
    }

    return data;
  }

  Future<List> listCommentaries(String token, String idMidia) async {
    List data;

    final response = await api.listCommentaries(token, idMidia);

    if (response.statusCode == 200) {
      data = json.decode(response.body)['response']['data'];
    } else {
      throw Error();
    }

    return data;
  }

  postCommentary(String token, String idMidia, String comment) async {
    Map data;

    final response = await api.postCommentary(token, idMidia, comment);

    if (response.statusCode == 201) {
      data = json.decode(response.body);
    }

    return data;
  }

  deleteCommentaries(String token, String idComentario) async {
    Map data;

    final response = await api.deleteCommentary(token, idComentario);

    if (response.statusCode == 200) {
      data = json.decode(response.body);
    }

    return data;
  }

  Future<List> getMideasUser(String token, Function changeMoment) async {
    List data;

    final response = await api.listMideasUser(token);
    if (response.statusCode == 200) {
      data = json.decode(response.body)['response'];
      changeMoment(data);
    }

    return data;
  }

  Future<Map> deleteMidea(String token, String idMidea) async {
    Map data;

    final response = await api.deleteMidea(token, idMidea);
    if (response.statusCode == 200) {
      data = json.decode(response.body);
    }
    print(response.body);
    return data;
  }

  Future<List> getMideasFamily(String idFamily, Function changeMoments) async {
    List data;

    final response = await api.getMideasFamily(_token, idFamily);
    if (response.statusCode == 200) {
      data = json.decode(response.body)['response'];
      changeMoments(data);
    }

    return data;
  }

  void verifyCode(String code, VoidCallback _success, VoidCallback _fail) {
    api.verifyCode(_emailRecovery, code).then((response) {
      if (response.statusCode == 202) {
        _success();
      } else {
        _fail();
      }
    });
  }

  void changePasswordCode(
      String password, VoidCallback _success, VoidCallback _fail) {
    api.changePasswordCode(_emailRecovery, _code, password).then((response) {
      if (response.statusCode != 200) {
        _fail();
      } else {
        _success();
      }
    });
  }

  verifyUsername(String username) {
    api.verifyUsername(username).then((response) {
      if (response.statusCode == 200) {
        _userController.sink.add(false);
      } else {
        _userController.sink.add(true);
      }
    }).catchError((err) {
      _userController.sink.add(true);
    });
  }

  Future uploadMidia(String nomeImg, String image, String tokenUser, String token) async {
    Map data;

    final response = await api.upload(tokenUser, token, nomeImg, image);

    if (response.statusCode == 201) {
      data = json.decode(response.body)['response'];
    }
    print(response.body);
    return data;
  }

  Future updateFotoPerfil(String token, String path, VoidCallback _success, VoidCallback _fail) async {
    print(path);

    final response = await api.updateFotoUser(token, path);

    if (response.statusCode == 200) {
      return _success();
    } else {
      return _fail();
    }
  }

  Future createFamily(Map<String, dynamic> data) async {
    data['token'] = _token;
    final response = await api.createFamily(data, _token);

    Map<String, dynamic> dados = {};

    if (response.statusCode == 201) {
      dados['token'] = json.decode(response.body)['response']['token'];
      dados['idFamily'] = json.decode(response.body)['response']['data']['_id'];
    }

    return dados;
  }

  findFamiliesUser() async {
    final response = await api.searchFamiliesUser(_token);

    List data;

    if (response.statusCode == 200) {
      data = json.decode(response.body)['response']['data'];
    }

    return data;
  }

  _save() async {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString("user", json.encode(_userData.toJson()));
      prefs.setString("logged", json.encode(_logged));
      prefs.setString("token", json.encode(_token));
    });
  }

  @override
  void dispose() {
    _userController.close();
    _momentsController.close();
    _updateFeedController.close();
    pageStream.close();
  }
}
