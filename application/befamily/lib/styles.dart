import 'package:flutter/material.dart';
class Styles {
  Color white() {
    return Colors.white;
  }

  Color primaryColorTransparent() {
    return Color.fromRGBO(250, 194, 11, 0.6);
  }

  Color primaryColorSolid() {
    return Color.fromRGBO(250, 194, 11, 1);
  }

  Color secondaryColor() {
    return Color.fromRGBO(47, 14, 71, 1);
  }

  InputDecoration inputDecorator(String nomeCampo, Icon icone) {
    return InputDecoration(
      border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(color: Colors.white)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(color: Colors.red)),
      errorStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
      labelStyle: TextStyle(color: Colors.white),
      labelText: nomeCampo,
      prefixIcon: icone,
      contentPadding: EdgeInsets.fromLTRB(15, 0, 10, 15),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(color: Colors.white)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(color: Colors.white)),
      counterText: '',
    );
  }

  Text textWhiteBold(String texto, double size) {
    return new Text(
      texto,
      style: new TextStyle(color: white(), fontWeight: FontWeight.bold, fontSize: size),
    );
  }

  Text textWhite(String texto, double size) {
    return new Text(
      texto,
      style: new TextStyle(color: white(), fontWeight: FontWeight.normal, fontSize: size),
    );
  }

    EdgeInsets textFormPadding(){
    return new EdgeInsets.fromLTRB(0, 0, 0, 8);
  }


  Icon iconGen(icon, double len){
    return new Icon(icon, size: len, color: white(),);
  }

  Padding paddingBot(double size){
    return new Padding(padding: EdgeInsets.only(bottom: size),);
  }

  Padding paddingLeft(double size){
    return new Padding(padding: EdgeInsets.only(left: size),);
  }

  Padding paddingRight(double size){
    return new Padding(padding: EdgeInsets.only(right: size),);
  }

  Padding paddingAll(double size){
    return new Padding(padding: EdgeInsets.all(size),);
  }
  
  Padding paddingTop(double size){
    return new Padding(padding: EdgeInsets.only(top: size),);
  }
  
}