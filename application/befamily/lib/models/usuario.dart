class UsuarioModel {
  String idUser;
  String nome;
  String sobrenome;
  String dataNascimento;
  String username;
  String profissao;
  String email;
  String telefone;
  String genero;
  String religiao;
  String descricao;
  String cidade;
  String senha;
  String fotoPerfil;

  UsuarioModel({this.nome, this.sobrenome, this.dataNascimento, this.username, this.profissao, this.email, this.telefone, this.genero, this.religiao, this.descricao, this.fotoPerfil, this.cidade, this.senha, this.idUser});
  
  factory UsuarioModel.fromJson(Map<String, dynamic> json) {
    return new UsuarioModel(
      idUser: json['idUser'],
      nome: json['nome'],
      sobrenome: json['sobrenome'],
      senha: json['senha'],
      dataNascimento: json['dataNascimento'],
      username: json['username'],
      profissao: json['profissao'],
      email: json['email'],
      telefone: json['telefone'],
      genero: json['genero'],
      religiao: json['religiao'],
      descricao: json['descricao'],
      cidade: json['cidade'],
      fotoPerfil: json['fotoPerfil']
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'idUser': idUser,
      'nome': nome,
      'sobrenome': sobrenome,
      'senha': senha,
      'dataNascimento': dataNascimento,
      'username': username,
      'profissao': profissao,
      'email': email,
      'telefone': telefone,
      'genero': genero,
      'religiao': religiao,
      'descricao': descricao,
      'cidade': cidade,
      'fotoPerfil': fotoPerfil
    };

  Map<String, dynamic> toJsonApi() =>
    {
      'nome': nome,
      'sobrenome': sobrenome,
      'senha': senha,
      'dataNascimento': dataNascimento,
      'username': username,
      'profissao': profissao,
      'email': email,
      'telefone': telefone,
      'genero': genero,
      'religiao': religiao,
      'descricao': descricao,
      'cidade': cidade,
      'fotoPerfil': fotoPerfil
    };

}
