class FeedModel {
  String idPublicacao;
  String legenda;
  List checkin;
  List like;
  int tipo;
  String path;
  DateTime data;
  String nome;
  String sobrenome;

  FeedModel({this.idPublicacao, this.legenda, this.checkin, this.like, this.tipo, this.path, this.data, this.nome, this.sobrenome});
  
  factory FeedModel.fromJson(Map<String, dynamic> json) {
    return new FeedModel(
      idPublicacao: json['idPublicacao'],
      legenda: json['legenda'],
      checkin: json['checkin'],
      like: json['like'],
      tipo: json['tipo'],
      path: json['path'],
      data: json['data'],
      nome: json['nome'],
      sobrenome: json['sobrenome']
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'idPublicacao': idPublicacao,
      'legenda': legenda,
      'checkin': checkin,
      'like': like,
      'tipo': tipo,
      'path': path,
      'data': data,
      'nome': nome,
      'sobrenome': sobrenome
    };

  Map<String, dynamic> toJsonApi() =>
    {
      'nome': nome,
      'legenda': legenda,
      'tipo': tipo.toString(),
    };
}
