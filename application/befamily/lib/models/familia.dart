class FamiliaModel {
  String idFamily;
  String nome;
  String descricao;
  int privacidade;
  List membros;
  List admins;
  String foto;
  String codigo;

  FamiliaModel({this.idFamily, this.nome, this.descricao, this.privacidade, this.membros, this.foto, this.admins, this.codigo});
  
  factory FamiliaModel.fromJson(Map<String, dynamic> json) {
    return new FamiliaModel(
      idFamily: json['idFamily'],
      nome: json['nome'],
      descricao: json['descricao'],
      privacidade: json['privacidade'],
      membros: json['membros'],
      foto: json['fotoFamilia'],
      codigo: json['codigo'],
      admins: json['admins']
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'idFamily': idFamily,
      'nome': nome,
      'descricao': descricao,
      'privacidade': privacidade,
      'membros': membros,
      'foto': foto,
      'codigo': codigo,
      'admins': admins
    };

  Map<String, dynamic> toJsonApi() =>
    {
      'nome': nome,
      'descricao': descricao,
      'privacidade': privacidade.toString(),
    };
}
