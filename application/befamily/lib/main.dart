import 'dart:async';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/pages/splash/splashScreen.dart';
import 'package:befamily/routes.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  StreamController connection = StreamController.broadcast();
  final style = Styles();

  @override
  Widget build(BuildContext context) {

    Connectivity().onConnectivityChanged.listen(
      (data) {
        connection.add(data);
      }
    );

    Widget basePage(Widget complete, BuildContext context) {
      return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        minimum: EdgeInsets.only(top: 20.0),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Scaffold(
              backgroundColor: style.secondaryColor(),
              body: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    complete
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    return StreamBuilder(
        stream: connection.stream,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              final temp = Container(
                alignment: Alignment.center,
                width: 200.0,
                height: 200.0,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 5.0,
                ),
              );
              return MaterialApp(
                home: basePage(temp, context),
              );
            default:
              if (snapshot.hasError) return Container();
              else {
                if (snapshot.data == ConnectivityResult.wifi || snapshot.data ==ConnectivityResult.mobile) {
                  return BlocProvider(
                    bloc: UsuarioBloc(),
                    child: BlocProvider(
                      bloc: FamiliaBloc(),
                      child: MaterialApp(
                        home: SplashScreen(),
                        routes: routes,
                      ),
                    )
                  );
                } else {
                  final temp = Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 15.0),
                          child: Icon(
                            Icons.signal_wifi_off,
                            color: Colors.white,
                            size: 50.0,
                          ),
                        ),
                        Text(
                          'Sem conexão',
                          style: TextStyle(fontSize: 32.0, color: Colors.white),
                        ),
                        Text(
                          'Conecte-se a internet para acessar! :)',
                          style: TextStyle(fontSize: 20.0, color: Colors.white),
                        )
                      ],
                    )
                  );
                  return MaterialApp(
                    home: basePage(temp, context),
                  );
                }
              }
          }
        },
      );
    // return BlocProvider(
    //     bloc: UsuarioBloc(),
    //     child: BlocProvider(
    //       bloc: FamiliaBloc(),
    //       child: MaterialApp(
    //         home: SplashScreen(),
    //         routes: routes,
    //       )
    //     )
    //   );
  }
}
