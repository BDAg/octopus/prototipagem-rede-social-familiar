import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:convert';

import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/pages/user/delegates/cidadesDelegate.dart';

import 'package:modal_progress_hud/modal_progress_hud.dart';

class CadastroPessoal extends StatefulWidget {
  @override
  _CadastroPessoalState createState() => new _CadastroPessoalState();
}

class _CadastroPessoalState extends State<CadastroPessoal> {

  final _keyFormPessoal = GlobalKey<FormState>();
  final _keyScaffold = GlobalKey<ScaffoldState>();

  Styles style = new Styles();
  File fotoPerfil;

  bool fotoCarregada;
  bool _saving = false;
  bool _disableButton = false;

  UsuarioBloc blocUsuario;

  Map<String, dynamic> _cidade;

  String filter;
  String base64Image;

  @override
  void initState(){
    super.initState();
    fotoPerfil = null;
    fotoCarregada = false;
  }

  @override
  Widget build(BuildContext context) {

    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);

    return Scaffold(
      key: _keyScaffold,
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/img/familia.jpg"),
                  fit: BoxFit.cover
                ),
              ),
            ),
            Scaffold(
              backgroundColor: style.primaryColorTransparent(),
              body: ModalProgressHUD(
                color: style.secondaryColor(),
                progressIndicator: CircularProgressIndicator(backgroundColor: style.primaryColorSolid(), valueColor: AlwaysStoppedAnimation(style.secondaryColor())),
                inAsyncCall: _saving,
                child: ListView(
                  padding: EdgeInsets.all(10.0),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 15.0),
                      child: Center(
                        child: formCadastroPessal(context)
                      )
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget formCadastroPessal(BuildContext context){
    return new Column(
        children: <Widget>[
          style.textWhiteBold("Escolher foto de perfil", 18),
          Form(
            key: _keyFormPessoal,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: style.textFormPadding(),
                  child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(child: _imagem(), decoration: BoxDecoration(border: Border.all(width: 1,color: style.white())),),
                        onTap: _mostrarPopUp,
                      ),
                  ]),
                ),
                Padding(
                  padding: style.textFormPadding(),
                  child: TextFormField(
                    decoration: style.inputDecorator("Sobre mim", Icon(Icons.question_answer, color: style.white(),)),
                    maxLines: 4,
                    keyboardType: TextInputType.multiline,
                    cursorWidth: 1,
                    cursorColor: style.white(),
                    onSaved: (val) => blocUsuario.userDataRegister.descricao = val,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ),
                Padding(
                  padding: style.textFormPadding(),
                  child: TextFormField(
                    decoration: style.inputDecorator("Religião", Icon(Icons.star, color: style.white(),)),
                    cursorWidth: 1,
                    cursorColor: style.white(),
                    onSaved: (val) => blocUsuario.userDataRegister.religiao = val,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ),
                Padding(
                  padding: style.textFormPadding(),
                  child: TextFormField(
                    decoration: style.inputDecorator("Profissão", Icon(Icons.work, color: style.white(),)),
                    cursorWidth: 1,
                    cursorColor: style.white(),
                    onSaved: (val) => blocUsuario.userDataRegister.profissao = val,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ),
                buttonCidade()
            ]
          )
        ),
        ButtonTheme(
          minWidth: 200.0,
          height: 40.0,
          child: RaisedButton.icon(
            color: style.secondaryColor(),
            onPressed: _disableButton ? null : _cadastrar,
            icon: Icon(
              Icons.done,
              color: style.white(),
            ),
            label: Text(
              'Finalizar',
              style: TextStyle(color: Colors.white),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
          )
        ),
      ],
    );
  }

  void _escolherCamera () async {
    fotoPerfil = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context, "OK");
    _mostrarPopUp();
  }

  void _escolherGaleria() async {
    fotoPerfil = await ImagePicker.pickImage(source: ImageSource.gallery);

    Navigator.pop(context, "OK");
    _mostrarPopUp();
  }

  void _carregar() {
    base64Image = base64Encode(fotoPerfil.readAsBytesSync());
    Navigator.pop(context, "OK");
    setState(() {
      fotoPerfil = fotoPerfil;
      fotoCarregada = true;
    });
  }
  
  void _excluir(){
    Navigator.pop(context, "OK");
    setState(() {
      fotoPerfil = null;
      fotoCarregada = false;
    });
  }

  void _success() {
    setState(() {
      _saving = false;
      _disableButton = true;
    });
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(
      SnackBar(
        content: Text('Cadastrado com sucesso!'),
        backgroundColor: style.secondaryColor(),
        duration: Duration(seconds: 3),
      )
    );
    Future.delayed(Duration(seconds: 3), () { Navigator.of(context).popAndPushNamed('/login'); });
  }

  void _fail() {
    setState(() {
      _saving = false;
    });
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('Falha ao cadastrar!'),
      backgroundColor: style.secondaryColor(),
      duration: Duration(seconds: 4),
    ));
  }

  _invalidForm() {
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('Informações inválidas, cidade não pode estar nulo!'),
      backgroundColor: style.secondaryColor(),
      duration: Duration(seconds: 4),
    ));
  }

  void _cadastrar(){
    if (_keyFormPessoal.currentState.validate() && _cidade != null) {
      _keyFormPessoal.currentState.save();
      blocUsuario.userDataRegister.cidade = _cidade["_id"];

      blocUsuario.createUser().then(
        (token) async {
          if (token != null) {
            if (fotoCarregada) {
              setState(() {
                _saving = true;
              });
              final newImage = await FlutterNativeImage.compressImage(fotoPerfil.path, quality: 40);
              base64Image = base64Encode(newImage.readAsBytesSync());
              blocUsuario.uploadMidia("fotoPerfil", base64Image, token, token).then(
                (dados) async {
                  print(dados);
                  await blocUsuario.updateFotoPerfil(token, dados['path'], _success, _fail);
                }
              );
            } else {
              _success();
            }
          } else {
            _fail();
          }
        }
      );
    } else {
      _invalidForm();
    }
  }

  Widget buttonCidade() {
    return ButtonTheme(
      minWidth: 200.0,
      height: 40.0,
      child: RaisedButton(
        color: style.secondaryColor(),
        onPressed: () async {
          await showSearch(
            delegate: CidadeDelegate(),
            context: context,
          ).then(
            (result) {
              setState(() {
                _cidade = json.decode(result);
              });
            }
          );
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Text( _cidade == null ? 'Cidade' : _cidade['nome'], style: TextStyle(color: Colors.white)),
      )
    );
  }

  Widget _imagem(){
    if (fotoPerfil != null){
      return Image.file(fotoPerfil, height: 100, width: 100, fit: BoxFit.fitWidth,); 
    }
    else
      return Icon(Icons.person_add, size: 100, color: style.white(),);
  }
  
  void _mostrarPopUp() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          backgroundColor: Colors.transparent,
          children: <Widget>[
            RaisedButton.icon(
              color: style.secondaryColor(),
              icon: Icon(Icons.photo_album, color: style.white(),),
              onPressed: _escolherGaleria,
              label: Text('Escolher da galeria', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            RaisedButton.icon(
              color: style.secondaryColor(),
              icon: Icon(Icons.photo_camera, color: style.white(),),
              onPressed: _escolherCamera,
              label: Text('Tirar Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            fotoPerfil == null
            ? Center(child:Text('Nenhuma imagem selecionada', style: TextStyle(fontWeight:FontWeight.bold, color: style.white()),))
            :
            Column(
              children: <Widget> 
              [
                Image.file(fotoPerfil, height: 200, width: 200,),
                fotoCarregada == true ?
                RaisedButton.icon(
                  color: style.secondaryColor(),
                  icon: Icon(Icons.photo_camera, color: style.white(),),
                  onPressed: _excluir,
                  label: Text('Excluir Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                  ),
                )                       : 
                RaisedButton.icon(
                  color: style.secondaryColor(),
                  icon: Icon(Icons.photo_camera, color: style.white(),),
                  onPressed: _carregar,
                  label: Text('Carregar Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24),
                  ),
                )
              ]
            )
          ],
        );
      }
    );
  }
}
