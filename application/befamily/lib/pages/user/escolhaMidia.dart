import 'package:befamily/pages/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:befamily/styles.dart';
import 'package:befamily/pages/user/publicar.dart';

class EscolhaMidia extends StatefulWidget {
  int tipoAtividade;

  EscolhaMidia({this.tipoAtividade});

  @override
  _EscolhaMidiaState createState() => new _EscolhaMidiaState(tipoAtividade: this.tipoAtividade);
}

class _EscolhaMidiaState extends State<EscolhaMidia> {
  int tipoAtividade;

  _EscolhaMidiaState({this.tipoAtividade});

  final style = Styles();
  File fotoPerfil;
  bool fotoCarregada;

  @override
  void initState(){
    super.initState();
    fotoPerfil = null;
    fotoCarregada = false;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Scaffold(
              backgroundColor: style.secondaryColor(),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 5),
                    child: Text(
                      'Como será o seu registro?',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: SizeConfig.blockSizeHorizontal * 8
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(Icons.add_a_photo, color: Colors.white, size: 70.0),
                        onTap: () async {
                          fotoPerfil = await ImagePicker.pickImage(source: ImageSource.camera);
                          if (fotoPerfil != null) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Publicar(data: {'fotoPerfil': fotoPerfil, 'tipoAtividade':tipoAtividade})
                              )
                            );
                          }
                        },
                      ),
                      GestureDetector(
                        child: Icon(Icons.camera_alt, color: Colors.white, size: 75.0),
                        onTap: () async {
                          fotoPerfil = await ImagePicker.pickImage(source: ImageSource.gallery);
                          if (fotoPerfil != null) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Publicar(data: {'fotoPerfil': fotoPerfil, 'tipoAtividade':tipoAtividade})
                              )
                            );
                          }
                        },
                      ),
                      // Padding(
                      //   padding: EdgeInsets.symmetric(horizontal: 20.0),
                      //   child: IconButton(
                      //     icon: Icon(Icons.local_movies, color: Colors.white, size: 70.0),
                      //     onPressed: () {},
                      //   ),
                      // ),
                    ],
                  )
                ],
              )
            )
          ]
        )
      )
    );
  }
}