import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  Styles style = new Styles();

  final _keyForm = GlobalKey<FormState>();
  final _keyScaffold = GlobalKey<ScaffoldState>();

  final _emailFocus = FocusNode();
  final _passwordFocus = FocusNode();

  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    final blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    final blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    _success(List families) {
      blocFamilia.setFamilies(families);
      if (blocFamilia.families.length != 0) {
        if (blocUsuario.firstTime) {
          Navigator.of(context).pushNamed('/emocoes');
        } else {
          Navigator.of(context).pushNamed('/home');
        }
      } else {
        Navigator.of(context).pushNamed('/join-familia');
      }
    }

    void _fail() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text('Falha ao entrar!'),
        backgroundColor: style.secondaryColor(),
        duration: Duration(seconds: 4),
      ));
    }

    void _login() {
      if (_keyForm.currentState.validate()) {
        _keyForm.currentState.save();
        blocUsuario.login(_email.replaceAll(' ', ''),
            _password.replaceAll(' ', ''), _success, _fail);
      }
    }

    Future<bool> _onWillPop() {
      return showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(32.0))),
                  title: new Text('Você tem certeza?'),
                  content: new Text('Você quer sair do App'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: new Text('Não'),
                    ),
                    new FlatButton(
                      onPressed: () => SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop'),
                      child: new Text('Sim'),
                    ),
                  ],
                ),
          ) ??
          false;
    }

    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          key: _keyScaffold,
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: false,
          body: SafeArea(
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage("assets/img/familia.jpg"),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Scaffold(
                    backgroundColor: style.primaryColorTransparent(),
                    body: Form(
                      key: _keyForm,
                      child: ListView(
                        padding: EdgeInsets.all(10.0),
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 140.0, bottom: 8.0),
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              focusNode: _emailFocus,
                              decoration: style.inputDecorator(
                                  'Usuário',
                                  Icon(
                                    Icons.person,
                                    color: Colors.white,
                                  )),
                              onSaved: (data) {
                                _email = data;
                              },
                              validator: (data) {
                                if (data.length < 6) {
                                  return 'E-mail ou nome de usuario Invalido';
                                }
                              },
                              onFieldSubmitted: (data) {
                                _emailFocus.unfocus();
                                FocusScope.of(context)
                                    .requestFocus(_passwordFocus);
                              },
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(bottom: 8.0),
                              child: TextFormField(
                                focusNode: _passwordFocus,
                                textInputAction: TextInputAction.done,
                                obscureText: true,
                                decoration: style.inputDecorator(
                                    'Senha',
                                    Icon(
                                      Icons.enhanced_encryption,
                                      color: Colors.white,
                                    )),
                                onSaved: (data) {
                                  _password = data;
                                },
                                validator: (data) {
                                  if (data.length < 6) {
                                    return 'Senha deve ter mais que 6 digitos';
                                  }
                                },
                                onFieldSubmitted: (data) {
                                  _passwordFocus.unfocus();
                                  _login();
                                },
                              )),
                          Padding(
                            padding: EdgeInsets.only(bottom: 24.0),
                            child: Column(
                              children: <Widget>[
                                ButtonTheme(
                                  minWidth: 200.0,
                                  height: 10.0,
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(24),
                                    ),
                                    onPressed: () {
                                      _login();
                                    },
                                    padding: EdgeInsets.all(12.0),
                                    color: style.secondaryColor(),
                                    child: Text('Entrar',
                                        style: TextStyle(color: Colors.white)),
                                  ),
                                ),
                                FlatButton(
                                  child: Text(
                                    'Esqueceu sua senha?',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .pushNamed('/esqueci-senha');
                                  },
                                ),
                                FlatButton(
                                  child: Text(
                                    'Ainda não entrou pra familia? Entre',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .pushNamed('/cadastro');
                                  },
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ));
  }
}
