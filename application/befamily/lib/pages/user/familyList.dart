import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/pages/user/widgets/familyItem.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class FamilyList extends StatefulWidget {
  @override
  _FamilyListState createState() => new _FamilyListState();
}

class _FamilyListState extends State<FamilyList> {
  final style = Styles();

  List<Map<String, dynamic>> data;

  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;

  @override
  Widget build(BuildContext context) {
    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
              backgroundColor: style.secondaryColor(),
              body: Column(children: <Widget>[
                Center(
                  child: Text('Famílias',
                      style: TextStyle(fontSize: 30.0, color: Colors.white)),
                  heightFactor: 2.5,
                ),
                Center(
                    child: Text('que você faz parte:',
                        style: TextStyle(fontSize: 25.0, color: Colors.white)),
                    heightFactor: 0.1),
                Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: FlatButton(
                      child: Text(
                        'Nova família? Clique aqui!',
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/join-familia');
                      },
                    )),
                Expanded(
                  child: FutureBuilder(
                    future: blocUsuario.findFamiliesUser(),
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                        case ConnectionState.none:
                          return Container(
                            alignment: Alignment.center,
                            width: 200.0,
                            height: 200.0,
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white),
                              strokeWidth: 5.0,
                            ),
                          );
                        default:
                          if (snapshot.hasError)
                            return Container();
                          else {
                            blocFamilia.setFamilies(snapshot.data);
                            return ListView.builder(
                                padding: EdgeInsets.all(10.0),
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, index) {
                                  return FamilyItemWidget(
                                      data: snapshot.data[index]);
                                });
                          }
                      }
                    },
                  ),
                ),
              ]))
        ])));
  }
}
