import 'package:befamily/pages/user/escolhaMidia.dart';
import 'package:befamily/styles.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

final style = Styles();

class NovaAtividade extends StatefulWidget {
  @override
  _NovaAtividadeState createState() => new _NovaAtividadeState();
}

class _NovaAtividadeState extends State<NovaAtividade> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
              backgroundColor: style.secondaryColor(),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('Escolha Uma atividade',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 20)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: GestureDetector(
                          child: Column(
                            children: <Widget>[
                              Icon(MdiIcons.walk,
                                  size: 90.0, color: style.primaryColorSolid()),
                              Text('Esporte',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.0))
                            ],
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    EscolhaMidia(tipoAtividade: 0)));
                          },
                        ),
                      ),
                      Expanded(
                        flex: 10,
                        child: GestureDetector(
                          child: Column(
                            children: <Widget>[
                              Icon(MdiIcons.food,
                                  size: 90.0, color: style.primaryColorSolid()),
                              Text('Refeição',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.0))
                            ],
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    EscolhaMidia(tipoAtividade: 1)));
                          },
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: GestureDetector(
                          child: Column(
                            children: <Widget>[
                              Icon(MdiIcons.book,
                                  size: 90.0, color: style.primaryColorSolid()),
                              Text('Cultura',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.0))
                            ],
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    EscolhaMidia(tipoAtividade: 2)));
                          },
                        ),
                      ),
                      Expanded(
                        flex: 10,
                        child: GestureDetector(
                          child: Column(
                            children: <Widget>[
                              Icon(MdiIcons.naturePeople,
                                  size: 90.0, color: style.primaryColorSolid()),
                              Text('Ar livre',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.0))
                            ],
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    EscolhaMidia(tipoAtividade: 3)));
                          },
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: GestureDetector(
                          child: Column(
                            children: <Widget>[
                              Icon(MdiIcons.balloon,
                                  size: 90.0, color: style.primaryColorSolid()),
                              Text('Festa',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.0))
                            ],
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    EscolhaMidia(tipoAtividade: 4)));
                          },
                        ),
                      ),
                      Expanded(
                        flex: 10,
                        child: GestureDetector(
                          child: Column(
                            children: <Widget>[
                              Icon(MdiIcons.trophy,
                                  size: 90.0, color: style.primaryColorSolid()),
                              Text('Conquista',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.0))
                            ],
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    EscolhaMidia(tipoAtividade: 5)));
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ))
        ])));
  }
}
