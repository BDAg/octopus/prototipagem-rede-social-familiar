import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/models/usuario.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CadastroUsuario extends StatefulWidget {
  @override
  _CadastroUsuarioState createState() => _CadastroUsuarioState();
}

class _CadastroUsuarioState extends State<CadastroUsuario> {
  Styles style = Styles();

  final _keyForm = GlobalKey<FormState>();
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _usernameController = TextEditingController();

  UsuarioModel cadastroUsuario = UsuarioModel();

  String _nome;
  String _sobrenome;
  String _nomeUsuario;
  String _email;
  String _dataDeNascimento;
  String _genero;
  String _telefone;
  String _senha;
  String _senhaConfirm;
  bool _termosDeServico = false;
  bool _submitButton = false;

  List<Map<String, String>> _generos = [
    {'sexo': 'Masculino', 'sigla': 'm'},
    {'sexo': 'Feminino', 'sigla': 'f'}
  ];

  UsuarioBloc blocUsuario;

  void termoDeServico(bool val) => setState(() => _termosDeServico = val);

  @override
  Widget build(BuildContext context) {
    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);

    return Scaffold(
      key: _keyScaffold,
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/img/familia.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
            Scaffold(
              backgroundColor: style.primaryColorTransparent(),
              body: ListView(
                padding: EdgeInsets.all(10.0),
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 15.0),
                      child: Center(child: form(context)))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget form(BuildContext context) {
    EdgeInsets paddingContainer = EdgeInsets.fromLTRB(20, 0, 20, 10);
    EdgeInsets paddingTextFormField = EdgeInsets.fromLTRB(0, 0, 0, 8);
    EdgeInsets scrollPading = EdgeInsets.fromLTRB(0, 5, 0, 0);

    return Container(
      padding: paddingContainer,
      child: Form(
        key: _keyForm,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: paddingTextFormField,
              child: TextFormField(
                focusNode: _focusNome,
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (term) {
                  _focusNome.unfocus();
                  FocusScope.of(context).requestFocus(_focusSobrenome);
                },
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                cursorWidth: 1,
                cursorColor: style.white(),
                decoration: style.inputDecorator(
                    "Nome", Icon(Icons.person, color: style.white())),
                onSaved: (val) => _nome = val,
                validator: (data) {
                  if (data.isEmpty || data.length < 3) {
                    return 'Nome de usuário é obrigatório!';
                  }
                },
              ),
            ),
            Padding(
              padding: paddingTextFormField,
              child: TextFormField(
                focusNode: _focusSobrenome,
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (term) {
                  _focusSobrenome.unfocus();
                  FocusScope.of(context).requestFocus(_focusNomedeUsuario);
                },
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                cursorWidth: 1,
                cursorColor: style.white(),
                decoration: style.inputDecorator(
                    "Sobrenome", Icon(Icons.person, color: style.white())),
                onSaved: (val) => _sobrenome = val,
                validator: (data) {
                  if (data.isEmpty || data.length < 3) {
                    return 'Sobrenome é obrigatório!';
                  }
                },
              ),
            ),
            Padding(
                padding: paddingTextFormField,
                child: StreamBuilder(
                  initialData: false,
                  stream: blocUsuario.outLogged,
                  builder: (context, snapshot) {
                    return TextFormField(
                      controller: _usernameController,
                      focusNode: _focusNomedeUsuario,
                      onFieldSubmitted: (term) {
                        _focusNomedeUsuario.unfocus();
                        FocusScope.of(context).requestFocus(_focusEmail);
                      },
                      textInputAction: TextInputAction.next,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                      cursorWidth: 1,
                      autovalidate: true,
                      cursorColor: style.white(),
                      decoration: style.inputDecorator("Username",
                          Icon(Icons.person_pin, color: style.white())),
                      onSaved: (val) => _nomeUsuario = val,
                      onEditingComplete: () {
                        blocUsuario.verifyUsername(_usernameController.text);
                      },
                      validator: (data) {
                        if (snapshot.data) {
                          return 'Username invalido';
                        } else if ((data == '' || data.contains(' ')) &&
                            snapshot.data == true) {
                          return 'Username é obrigatório e não pode conter espaços!';
                        } else if ((data == '' || data.contains(' ')) &&
                            _submitButton == true) {
                          return 'Username é obrigatório e não pode conter espaços!';
                        }
                        // else if ((data.length < 6 && snapshot.data == true)) {
                        //   return 'Username tem que ter mais que 6 caracteres';
                        // }
                      },
                    );
                  },
                )),
            Padding(
                padding: paddingTextFormField,
                child: TextFormField(
                  focusNode: _focusEmail,
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (term) {
                    blocUsuario.verifyUsername(_usernameController.text);
                    _focusEmail.unfocus();
                    FocusScope.of(context).requestFocus(_focusDataDeNascimento);
                  },
                  scrollPadding: scrollPading,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  cursorWidth: 1,
                  cursorColor: style.white(),
                  keyboardType: TextInputType.emailAddress,
                  decoration: style.inputDecorator(
                      "Email", Icon(Icons.email, color: style.white())),
                  onSaved: (val) => _email = val,
                  validator: (data) {
                    if (data.isEmpty ||
                        !data.contains('@') ||
                        !data.contains('.')) {
                      return 'E-mail é obrigatório!';
                    }
                  },
                )),
            Padding(
                padding: paddingTextFormField,
                child: TextFormField(
                  focusNode: _focusDataDeNascimento,
                  maxLength: 10,
                  keyboardType: TextInputType.number,
                  scrollPadding: scrollPading,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  cursorWidth: 1,
                  cursorColor: style.white(),
                  decoration: style.inputDecorator("Data de nascimento",
                      Icon(Icons.cake, color: style.white())),
                  onSaved: (val) => _dataDeNascimento = new DateTime(
                          int.parse(val.split('/')[2]),
                          int.parse(val.split('/')[1]),
                          int.parse(val.split('/')[0]))
                      .millisecondsSinceEpoch
                      .toString(),
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly,
                    _birthDate
                  ],
                  validator: (data) {
                    if (data.isEmpty) {
                      return 'Data de nascimento é obrigatório!';
                    }
                  },
                )),
            Padding(
                padding: paddingTextFormField,
                child: FormField<String>(
                  builder: (FormFieldState<String> state) {
                    return InputDecorator(
                      // isFocused: true,
                      decoration: style.inputDecorator(
                          "Gênero", Icon(Icons.group, color: style.white())),
                      baseStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                      isEmpty: _genero == null,
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          iconSize: 0,
                          isDense: true,
                          value: _genero,
                          onChanged: (String newValue) {
                            setState(() {
                              cadastroUsuario.genero = newValue;
                              _genero = newValue;
                              state.didChange(newValue);
                            });
                          },
                          items: _generos.map((value) {
                            return DropdownMenuItem(
                              value: value['sigla'],
                              child: Text(value['sexo'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18)),
                            );
                          }).toList(),
                        ),
                      ),
                    );
                  },
                  validator: (data) {
                    return data != null ? null : 'Gênero é obrigatório';
                  },
                )),
            Padding(
                padding: paddingTextFormField,
                child: TextFormField(
                  focusNode: _focusTelefone,
                  onFieldSubmitted: (term) {
                    _focusTelefone.unfocus();
                    FocusScope.of(context).requestFocus(_focusSenha);
                  },
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.phone,
                  scrollPadding: scrollPading,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  cursorWidth: 1,
                  cursorColor: style.white(),
                  decoration: style.inputDecorator(
                      "Telefone", Icon(Icons.phone, color: style.white())),
                  onSaved: (val) => _telefone = val,
                  validator: (data) {
                    if (data.isEmpty) {
                      return 'Telefone é obrigatório!';
                    }
                  },
                )),
            Padding(
                padding: paddingTextFormField,
                child: TextFormField(
                  focusNode: _focusSenha,
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (term) {
                    _focusSenha.unfocus();
                    FocusScope.of(context).requestFocus(_focusConfirmarSenha);
                  },
                  scrollPadding: scrollPading,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  cursorWidth: 1,
                  cursorColor: style.white(),
                  decoration: style.inputDecorator(
                      "Senha", Icon(Icons.lock, color: style.white())),
                  obscureText: true,
                  onSaved: (val) => _senha = val,
                  validator: (data) {
                    if (data.isEmpty || data.length < 6) {
                      return 'Senha muito curta ou inexistente';
                    }
                  },
                )),
            Padding(
                padding: paddingTextFormField,
                child: TextFormField(
                  focusNode: _focusConfirmarSenha,
                  textInputAction: TextInputAction.done,
                  scrollPadding: scrollPading,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  cursorWidth: 1,
                  cursorColor: style.white(),
                  decoration: style.inputDecorator("Confirmar senha",
                      Icon(Icons.lock, color: style.white())),
                  obscureText: true,
                  onSaved: (val) => _senhaConfirm = val,
                  validator: (data) {
                    if (data.isEmpty) {
                      return 'Senha é obrigatório!';
                    }
                  },
                )),
            Column(children: <Widget>[
              FormField(
                builder: (state) {
                  return CheckboxListTile(
                    value: _termosDeServico,
                    onChanged: termoDeServico,
                    title: style.textWhite("Termos de serviço", 15),
                    controlAffinity: ListTileControlAffinity.leading,
                    subtitle: style.textWhite(
                        "Aceito e concordo com os temos de serviço!", 12),
                    activeColor: style.secondaryColor(),
                  );
                },
                validator: (data) {
                  if (data == false) {
                    return 'Termos de serviço é obrigatório';
                  }
                },
              ),
            ]),
            Padding(
              padding: EdgeInsets.only(bottom: 25.0),
              child: ButtonTheme(
                  minWidth: 200.0,
                  height: 30.0,
                  child: RaisedButton.icon(
                    color: style.secondaryColor(),
                    onPressed: _fetchPost,
                    icon: Icon(
                      Icons.done,
                      color: style.white(),
                    ),
                    label: Text(
                      'Avançar',
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }

  void _fetchPost() {
    this._submitButton = true;
    final form = _keyForm.currentState;
    if (form.validate() && _termosDeServico) {
      form.save();
      if (_senha == _senhaConfirm) {
        cadastroUsuario.nome = _nome;
        cadastroUsuario.sobrenome = _sobrenome;
        cadastroUsuario.username = _nomeUsuario;
        cadastroUsuario.email = _email;
        cadastroUsuario.dataNascimento = _dataDeNascimento;
        cadastroUsuario.genero = _genero;
        cadastroUsuario.telefone = _telefone;
        cadastroUsuario.senha = _senha;
        this.blocUsuario.setUserDataRegister(
            cadastroUsuario, _successVerifyEmail, _failVerifyEmail);
      } else {
        FocusScope.of(context).requestFocus(new FocusNode());
        _keyScaffold.currentState.showSnackBar(SnackBar(
          content: Text('Senhas não correspondem!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 4),
        ));
      }
    } else {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text('Confira os dados!'),
        backgroundColor: style.secondaryColor(),
        duration: Duration(seconds: 4),
      ));
    }
  }

  _successVerifyEmail() {
    Navigator.of(context).popAndPushNamed('/cadastro-pessoa');
  }

  _failVerifyEmail() {
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('E-mail invalido, já utilizado!'),
      backgroundColor: style.secondaryColor(),
      duration: Duration(seconds: 4),
    ));
  }

  static final _UsNumberTextInputFormatter _birthDate =
      _UsNumberTextInputFormatter();
  final _focusNome = FocusNode();
  final _focusNomedeUsuario = FocusNode();
  final _focusEmail = FocusNode();
  final _focusDataDeNascimento = FocusNode();
  final _focusTelefone = FocusNode();
  final _focusSenha = FocusNode();
  final _focusConfirmarSenha = FocusNode();
  final _focusSobrenome = FocusNode();
}

class _UsNumberTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final int newTextLength = newValue.text.length;
    int selectionIndex = newValue.selection.end;
    int usedSubstringIndex = 0;
    final StringBuffer newText = StringBuffer();
    if (newTextLength >= 3) {
      newText.write(newValue.text.substring(0, usedSubstringIndex = 2) + '/');
      if (newValue.selection.end >= 2) selectionIndex++;
    }
    if (newTextLength >= 5) {
      newText.write(newValue.text.substring(2, usedSubstringIndex = 4) + '/');
      if (newValue.selection.end >= 4) selectionIndex++;
    }
    if (newTextLength >= 9) {
      newText.write(newValue.text.substring(4, usedSubstringIndex = 8));
      if (newValue.selection.end >= 8) selectionIndex++;
    }
    if (newTextLength >= usedSubstringIndex)
      newText.write(newValue.text.substring(usedSubstringIndex));
    return TextEditingValue(
      text: newText.toString(),
      selection: TextSelection.collapsed(offset: selectionIndex),
    );
  }
}
