import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:befamily/styles.dart';

class ConfigFamilia extends StatefulWidget {
  Map data;

  ConfigFamilia(this.data);

  @override
  _ConfigFamiliaState createState() => new _ConfigFamiliaState(data);
}

class _ConfigFamiliaState extends State<ConfigFamilia> {
  Map data;

  _ConfigFamiliaState(this.data);

  final style = Styles();
  final _keyScaffold = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
            key: _keyScaffold,
              backgroundColor: style.secondaryColor(),
              body: Container(
                  padding: EdgeInsets.fromLTRB(10.0, 25.0, 10.0, 10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('Configuração',
                              style: TextStyle(
                                  color: Colors.white, 
                                  fontSize: 30.0))
                        ],
                      ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: <Widget>[
                                style.textWhiteBold("O código da sua família é:", 27),
                                PhysicalModel(
                                  shadowColor: style.primaryColorSolid(),
                                  elevation: 15,
                                  color: style.secondaryColor(),
                                  child:
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child:
                                    GestureDetector(
                                      child: style.textWhiteBold(data['_id'], 18),
                                      onTap: (){
                                        Clipboard.setData(new ClipboardData(text: data['_id']));
                                        _keyScaffold.currentState.showSnackBar(
                                          new SnackBar(content: Text("Código copiado", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)), backgroundColor: style.primaryColorSolid()),
                                        );
                                      },
                                    )
                                  )
                                ),
                              ],
                            ),
                          )
                        )
                      ]
                    )
                  )
                )
              ]
            )
          )
        );
  }
}