import 'package:flutter/material.dart';
import 'package:bloc_pattern/bloc_pattern.dart';

import 'package:befamily/styles.dart';
import 'package:befamily/blocs/usuarioBloc.dart';

class RedefinirSenhaEmail extends StatefulWidget {
  @override
  _RedefinirSenhaEmailState createState() => _RedefinirSenhaEmailState();
}

class _RedefinirSenhaEmailState extends State<RedefinirSenhaEmail> {
  TextEditingController _passwordConfirmedController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _keyScaffold = GlobalKey<ScaffoldState>();

  Styles style = Styles();

  final _passwordConfirmedFocus = FocusNode();
  final _passwordFocus = FocusNode();

  String _passwordConfirmed;
  String _password;

  @override
  Widget build(BuildContext context) {

    final blocUsuario = BlocProvider.of<UsuarioBloc>(context);

    senhasInvalidas() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('Senhas não correspondem!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 4),
        )
      );
    }

    void _successChange() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('Senha Alterada!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 3),
        )
      );
      Future.delayed(Duration(seconds: 3), () { Navigator.of(context).popAndPushNamed('/login'); });
    }

    void _failChange() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('Não foi possível alterar a senha! Tente mais tarde!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 3),
        )
      );
    }

    senhasValidas() {
      blocUsuario.changePasswordCode(_password, _successChange, _failChange);
    }

    return Scaffold(
      key: _keyScaffold,
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/img/familia.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
            Scaffold(
              backgroundColor: style.primaryColorTransparent(),
              body: Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.all(10.0),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 120.0),
                      child: TextFormField(
                        textInputAction: TextInputAction.next,
                        focusNode: _passwordFocus,
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        decoration: style.inputDecorator("Nova senha", Icon(Icons.looks_one, color: Colors.white)),
                        controller: _passwordController,
                        validator: (value) {
                          if (value.isEmpty || value.length < 6) {
                            return "Insina seu/sua nova senha!";
                          }
                        },
                        onSaved: (data) {
                          _password = data;
                        },
                        onFieldSubmitted: (data) {
                          _passwordFocus.unfocus();
                          FocusScope.of(context).requestFocus(_passwordConfirmedFocus);
                        },
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        focusNode: _passwordConfirmedFocus,
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        decoration: style.inputDecorator("Confirmar nova senha", Icon(Icons.looks_one, color: Colors.white)),
                        controller: _passwordConfirmedController,
                        validator: (value) {
                          if (value.isEmpty || value.length < 6) {
                            return "Insina seu/sua nova senha!";
                          }
                        },
                        onSaved: (data) {
                          _passwordConfirmed = data;
                        },
                        onFieldSubmitted: (data) {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            if (_password !=_passwordConfirmed) {
                              senhasInvalidas();
                            } else {
                              senhasValidas();
                            }
                          }
                        },
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: ButtonTheme(
                        minWidth: 200.0,
                        height: 10.0,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              if (_password !=_passwordConfirmed) {
                                senhasInvalidas();
                              } else {
                                senhasValidas();
                              }
                            }
                          },
                          padding: EdgeInsets.all(12.0),
                          color: style.secondaryColor(),
                          child: Text('Entrar', style: TextStyle(color: Colors.white)),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ),
          ],
        ),
      ),
    );
  }
}
