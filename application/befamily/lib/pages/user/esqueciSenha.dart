import 'package:befamily/styles.dart';
import 'package:flutter/material.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:befamily/blocs/usuarioBloc.dart';

class EsqueciSenha extends StatefulWidget {
  @override
  _EsqueciSenhaState createState() => _EsqueciSenhaState();
}

class _EsqueciSenhaState extends State<EsqueciSenha> {

  final _formEmailKey = GlobalKey<FormState>();
  final _formCodeKey = GlobalKey<FormState>();
  final _keyScaffold = GlobalKey<ScaffoldState>();

  Styles style = new Styles();

  String _email;
  String _code;

  @override
  Widget build(BuildContext context) {

    final blocUsuario = BlocProvider.of<UsuarioBloc>(context);

    void _successEmail() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('E-mail Enviado com sucesso!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 4),
        )
      );
    }

    void _failEmail() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('E-mail Falhou!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 4),
        )
      );
    }

    void _successCode() {
      blocUsuario.setCode(_code);
      Navigator.of(context).popAndPushNamed('/redefinir-senha-email');
    }

    void _failCode() {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('Código Invalido!'),
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 4),
        )
      );
    }

    Widget backgroundImage = Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage("assets/img/familia.jpg"), fit: BoxFit.cover),
      ),
    );

    ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _loading(String msg) {
      return _keyScaffold.currentState.showSnackBar(
        new SnackBar(
          backgroundColor: style.secondaryColor(),
          duration: Duration(seconds: 4),
          content: Row(
            children: <Widget>[
                new CircularProgressIndicator(),
                new Text("  $msg...")
            ],
          ),
        )
      );
    }

    return Scaffold(
      key: _keyScaffold,
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            backgroundImage,
            Scaffold(
                backgroundColor: style.primaryColorTransparent(),
                body: ListView(
                  padding: EdgeInsets.all(10.0),
                  children: <Widget>[
                    Form(
                      key: _formEmailKey,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 120.0, 8.0, 8.0),
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              decoration: style.inputDecorator("E-mail", Icon(Icons.person, color: Colors.white)),
                              validator: (value) {
                                if (!value.contains('@') || !value.contains('.')) {
                                  return "Insina seu/sua E-mail!";
                                }
                              },
                              onSaved: (data) {
                                _email = data;
                              },
                              onFieldSubmitted: (data) {
                                if (_formEmailKey.currentState.validate()) {
                                  _loading('Enviando');
                                  _formEmailKey.currentState.save();
                                  blocUsuario.sendMail(_email, _successEmail, _failEmail);
                                }
                              },
                            )
                          ),
                          ButtonTheme(
                            minWidth: 200.0,
                            height: 10.0,
                            child: RaisedButton(
                              child: Text(
                                'Enviar código',
                                style: TextStyle(color: Colors.white),
                              ),
                              padding: EdgeInsets.all(12),
                              color: style.secondaryColor(),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: () {
                                if (_formEmailKey.currentState.validate()) {
                                  _loading('Enviando');
                                  _formEmailKey.currentState.save();
                                  blocUsuario.sendMail(_email, _successEmail, _failEmail);
                                }
                              },
                            ),
                          )
                        ],
                      )
                    ),
                    Form(
                      key: _formCodeKey,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 50.0, 8.0, 8.0),
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              decoration: style.inputDecorator("Código", Icon(Icons.looks_one, color: Colors.white)),
                              validator: (data) {
                                if (data.length < 6 || data.length > 6) {
                                  return 'Tamanho inválido';
                                }
                              },
                              onSaved: (data) {
                                _code = data;
                              },
                              onFieldSubmitted: (data) {
                                if (_formCodeKey.currentState.validate()) {
                                  _loading('Validando');
                                  _formCodeKey.currentState.save();
                                  blocUsuario.verifyCode(_code, _successCode, _failCode);
                                }
                              },
                            )
                          ),
                          ButtonTheme(
                            minWidth: 200.0,
                            height: 10.0,
                            child: RaisedButton(
                              child: Text(
                                'Enviar código',
                                style: TextStyle(color: Colors.white),
                              ),
                              padding: EdgeInsets.all(12),
                              color: style.secondaryColor(),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: () {
                                if (_formCodeKey.currentState.validate()) {
                                  _loading('Validando');
                                  _formCodeKey.currentState.save();
                                  blocUsuario.verifyCode(_code, _successCode, _failCode);
                                }
                              },
                            ),
                          )
                        ],
                      )
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
