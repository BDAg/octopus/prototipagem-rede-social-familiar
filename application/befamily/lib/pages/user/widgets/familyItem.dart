import 'package:flutter/material.dart';

class FamilyItemWidget extends StatelessWidget {
  Map<String, dynamic> data;

  FamilyItemWidget({this.data});

  @override
  Widget build(BuildContext context) {
    imagem() {
      if (data['foto'] == '') {
        return Icon(
          Icons.people,
          size: 70.0,
          color: Colors.white,
        );
      } else {
        return Image.network('http://35.231.220.66:4000/images/' + data['foto'],
            width: 70.0, height: 70.0, fit: BoxFit.cover);
      }
    }

    return Padding(
      padding: EdgeInsets.only(bottom: 15.0),
      child: GestureDetector(
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: imagem(),
            ),
            Text(data['nome'],
                style: TextStyle(color: Colors.white, fontSize: 20.0))
          ],
        ),
        onTap: () {
          Navigator.of(context).pushNamed('/perfil-familia', arguments: data);
        },
      ),
    );
  }
}
