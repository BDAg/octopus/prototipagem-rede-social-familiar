import 'package:befamily/pages/sizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:befamily/styles.dart';
// import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/pages/user/widgets/telaComentarios.dart';
import 'package:vector_math/vector_math_64.dart' as math;

class FeedPublicacoesWidget extends StatefulWidget {
  Map<String, dynamic> dataFeed;
  String userID;
  final keyScaffold;
  FeedPublicacoesWidget({this.dataFeed, this.userID, this.keyScaffold});

  @override
  _FeedPublicacoesState createState() => new _FeedPublicacoesState(
      dataFeed: this.dataFeed, user: userID, keyScaffold: this.keyScaffold);
}

class _FeedPublicacoesState extends State<FeedPublicacoesWidget>
  with SingleTickerProviderStateMixin {
  bool iconLike;
  bool isOwner;
  double previousScale;
  Map<String, dynamic> dataFeed;
  String user;
  Styles style = new Styles();
  final keyScaffold;
  _FeedPublicacoesState({this.dataFeed, this.user, this.keyScaffold});
  UsuarioBloc usuarioBloc;

  bool disableButton;
  bool like;
  bool reported;

  Map commentarios;
  double width;
  double height;
  double imageWidth;
  double imageHeight;
  List likePub=[];
  double scale = 1;
  _like() async {
    var data = await usuarioBloc.like(usuarioBloc.token, dataFeed["_id"]);
      if (!data.isEmpty) {
        if (this.like) {
          setState(() {
            this.like = false;
          });
          int index = 0;
          for(var each in this.likePub){
            if(each["_id"] == user){
              print(each["_id"]);
              print(user);
              this.likePub.removeAt(index);
              return true;
            }
            index+=1;
          }
        } else
          setState(() {
            this.like = true;
          });
          this.likePub.insert(0, {"_id":user, "nome":usuarioBloc.userData.nome, "sobrenome": usuarioBloc.userData.sobrenome, "username": usuarioBloc.userData.username, "foto":usuarioBloc.userData.fotoPerfil});
          return true;
      } else
        print("Deu ruim!");
  }

  @override
  initState() {
    super.initState();
    if(this.likePub.length==0){
      this.likePub = dataFeed["like"];
    }
    print(dataFeed["nome"]);


    this.disableButton = false;
    this.reported = false;
    this.iconLike = false;
    this.like = false;
    for (var likes in this.likePub){
      if (likes["_id"] == user) {
        this.like = true;
        break;
      }
    }
    this.isOwner = false;
    if (dataFeed["idUsuario"] == user) {
      this.isOwner = true;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // this.width = MediaQuery.of(context).size.width;
    // this.height = MediaQuery.of(context).size.height;
    this.width = SizeConfig.screenWidth;
    this.height = SizeConfig.safeVertical;
    this.imageWidth = dataFeed["width"].toDouble();
    this.imageHeight = dataFeed["height"].toDouble();
    double relacao;
    
    print(dataFeed["nome"]);
    print(this.imageHeight);
    print(this.imageWidth);
    relacao = this.width / this.imageWidth;

    double containerHeight;
    
    if(this.imageWidth > this.imageHeight){
      containerHeight = this.imageHeight * relacao;
      // print(relacaoHeight);
      // double relacaoHeight = this.height/this.imageHeight * relacaoHeight;
      // if(relacaoHeight > this.height) {
      //   double relacaoWidHei = this.imageWidth/this.imageHeight;
      //   containerHeight = this.height*relacaoHeight * relacaoWidHei;
      // }
    }
    else{
      containerHeight = this.imageHeight * relacao;
    }
    print(width);
    print(containerHeight);

    this.usuarioBloc = BlocProvider.of<UsuarioBloc>(context);
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: style.white()),
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                    child: dataFeed["foto"] == ''
                        ? new Icon(Icons.person, color: style.white())
                        : CachedNetworkImage(
                            imageUrl: "http://35.231.220.66:4000/images/" +
                                dataFeed["foto"],
                            placeholder: (context, url) =>
                                new CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.info, color: style.white()),
                            fit: BoxFit.fitWidth,
                            width: 25,
                            height: 25),
                    // Image.network("http://35.231.220.66:4000/images/"+dataFeed["foto"],width: 25, height: 25, fit: BoxFit.fitWidth,),
                  ),
                ),
                style.paddingRight(10.0),
                style.textWhiteBold(
                    dataFeed["nome"] + " " + dataFeed["sobrenome"], 15),
              ],
            ),
            isOwner ?
            Column(children: <Widget>[
              IconButton(
                icon: style.iconGen(Icons.more_horiz, 25),
                onPressed: (() async {
                  await showDialog(
                      context: context,
                      builder: (context) {
                        return new CupertinoAlertDialog(
                          content: new MidiaDialog(
                              dataFeed: dataFeed,
                              keyScaffold: keyScaffold,
                              usuarioBloc: usuarioBloc,
                              isOwner: isOwner),
                        );
                      });
                }),
              )
            ])
            : Column()
          ],
        ),
        style.paddingBot(5),
        Container(
            height: containerHeight,
            width: this.width,
            child:
             GestureDetector(
              child:
                CachedNetworkImage(
                    height: containerHeight,
                    width: this.width,
                    imageUrl: 'http://35.231.220.66:4000/images/' + dataFeed["path"],
                    placeholder: (context, url) =>
                        new 
                        Center(
                          child:
                            Container(
                              width: 25,
                              height: 25,
                              child: CircularProgressIndicator(strokeWidth: 1),
                            )
                          )
                        ,
                    errorWidget: (context, url, error) => new Icon(Icons.error),
                    fit: BoxFit.fitWidth,
                ),
                onDoubleTap: _like,
                onTap: (){
                  showDialog(
                    context: context,
                    builder: (context){
                      return CupertinoDialogAction(
                        // elevation: 1,
                        child: 
                        Container(
                          width: this.width,
                          height: this.height,
                          child: CachedNetworkImage(
                            imageUrl:'http://35.231.220.66:4000/images/' + dataFeed["path"],
                            fit: BoxFit.fitWidth,
                          )
                        )
                      );
                    }
                  );
                },
                onLongPress: (() {
                  }
                )
          )
        ),

        Container(
          height: 27.0,
          child:
          Row(
            children: <Widget>[
              IconButton(
                  icon: like
                      ? style.iconGen(Icons.favorite, 25)
                      : style.iconGen(Icons.favorite_border, 25),
                  onPressed: () {
                    _like();
                  }),
              IconButton(
                  icon: style.iconGen(Icons.insert_comment, 25),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => TelaComentarios(dataFeed)));
                  }),
              // IconButton(
              //   icon: style.iconGen(Icons.share, 25),
              //   onPressed: (() {
              //     // _shareImage(dataFeed["path"]);
              //   }),
              // ),
            ],
          )
        )
        ,
        style.paddingTop(5),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            style.paddingLeft(5),
            Container(
              height: 14,
              child: 
                style.textWhite(this.likePub.length.toString()+" curtidas", 10)
            )
          ],
        ),
        Column(
          children: <Widget>[
            dataFeed["legenda"] != ''
            ? Row(children: <Widget>[
                Flexible(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        "@" + dataFeed["username"] + ": " + dataFeed["legenda"],
                        style: TextStyle(
                            fontSize: 15,
                            color: style.white(),
                            fontStyle: FontStyle.italic)),
                  ],
                ))
              ])
            : Container(
              height: 0,
              ),
            Container(
              height: 17,
              child:
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ["1"].length > 0
                  ? FlatButton(
                      child: Text(
                        dataFeed["comment"].toString() + " comentário(s)",
                        style: TextStyle(
                            fontSize: 15,
                            color: style.white(),
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => TelaComentarios(dataFeed)));
                      })
                  : Text("Nenhum comentário",
                      style: TextStyle(
                          fontSize: 15,
                          color: style.white(),
                          fontStyle: FontStyle.italic)
                  ),
                ] 
              )
            ),
          ],
        ),
        style.paddingBot(10.0),
      ],
    );
  }
  // _shareImage(image) async {
  //   final ByteData bytes = await rootBundle.load(image);
  //   await Share.file('esys image', 'esys.jpg', bytes.buffer.asUint8List(), 'image/jpg');
  // }
}

class MidiaDialog extends StatefulWidget {
  Map<String, dynamic> dataFeed;
  String userID;
  final keyScaffold;
  UsuarioBloc usuarioBloc;
  bool isOwner;
  MidiaDialog(
      {this.dataFeed, this.keyScaffold, this.usuarioBloc, this.isOwner});

  @override
  MidiaDialogState createState() => new MidiaDialogState(
      dataFeed: this.dataFeed,
      keyScaffold: this.keyScaffold,
      usuarioBloc: this.usuarioBloc,
      isOwner: this.isOwner);
}

class MidiaDialogState extends State<MidiaDialog> {
  bool disableButton;
  Map<String, dynamic> dataFeed;
  final keyScaffold;
  UsuarioBloc usuarioBloc;
  bool isOwner;
  String userID;
  MidiaDialogState(
      {this.dataFeed, this.keyScaffold, this.usuarioBloc, this.isOwner});
  bool reportByExplict;
  bool reportByActivitie;
  bool reportByAnyElse;
  String reportCase;

  @override
  initState() {
    super.initState();
    this.disableButton = false;
    reportByExplict = false;
    reportByActivitie = true;
    reportByAnyElse = false;
    reportCase = "Atividade inválida";
  }

  Styles style = new Styles();
  show() {
    return Column(children: <Widget>[
      !isOwner
          ? Container()
          : Padding(
              padding: EdgeInsets.all(10),
              child: RaisedButton.icon(
                label: style.textWhiteBold(
                    disableButton ? "Excluindo..." : "Excluir foto", 15),
                textColor: style.white(),
                icon: disableButton
                    ? style.iconGen(Icons.access_time, 25)
                    : style.iconGen(Icons.delete, 25),
                color: disableButton ? Colors.blueGrey : style.secondaryColor(),
                onPressed: (() async {
                  if (!disableButton) {
                    setState(() {
                      disableButton = true;
                    });
                    Map resData = await usuarioBloc.deleteMidea(
                        usuarioBloc.token, dataFeed["_id"]);
                    if (resData != null) {
                      keyScaffold.currentState.showSnackBar(SnackBar(
                        content: Text('Publicação excluída',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                        backgroundColor: style.primaryColorSolid(),
                        duration: Duration(seconds: 3),
                      ));
                      if (Navigator.of(context).canPop()) {
                        Navigator.of(context).popAndPushNamed('/home');
                      }
                    } else {
                      keyScaffold.currentState.showSnackBar(SnackBar(
                        content: Text('Erro ao excluir publicação',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                        backgroundColor: style.primaryColorSolid(),
                        duration: Duration(seconds: 3),
                      ));
                      if (Navigator.of(context).canPop()) {
                        Navigator.of(context).pop();
                      }
                    }
                  }
                }),
              ))
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return show();
  }
}

// _reported() {
//   setState(() {
//     if (this.like)
//       this.like = false;
//     else
//       this.like = true;
//   });
// }

// reportPopUp(dataFeed) {
//     String nome = dataFeed["nome"].toString();
//     GlobalKey<FormState> _keyReport = new GlobalKey<FormState>();
//     Navigator.pop(context, "");
//           return SimpleDialog(
//             backgroundColor: style.primaryColorTransparent(),
//             title: style.textWhiteBold("Denunciar foto de " + nome, 20),
//             children: <Widget>[
//               Padding(
//                 padding: EdgeInsets.only(left: 10),
//                 child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: <Widget>[
//                       style.textWhiteBold("Conteúdo explícito", 15),
//                       reportItems("explicito", this.reportByExplict, dataFeed),
//                     ]),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(left: 10),
//                 child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: <Widget>[
//                       style.textWhiteBold("Não é uma atividade", 15),
//                       reportItems(
//                           "atividade", this.reportByActivitie, dataFeed),
//                     ]),
//               ),
//               Padding(
//                   padding: EdgeInsets.only(left: 10),
//                   child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: <Widget>[
//                         style.textWhiteBold("Outro", 15),
//                         reportItems("outro", this.reportByAnyElse, dataFeed),
//                       ])),
//               this.reportByAnyElse
//                   ? Form(
//                       key: _keyReport,
//                       child: Padding(
//                         padding: EdgeInsets.only(left: 10, right: 10),
//                         child: TextFormField(
//                           decoration: style.inputDecorator("", null),
//                           onSaved: (value) => this.reportCase = value,
//                           validator: (val) {
//                             if (val.isEmpty) {
//                               return "Digite sua resposta";
//                             }
//                           },
//                         ),
//                       ))
//                   : Container(
//                       height: 23,
//                     ),
//               dataFeed["Reported"] == false
//                   ? Padding(
//                       padding: EdgeInsets.only(right: 10, left: 10),
//                       child: FlatButton(
//                         child: style.textWhiteBold(
//                             "Denunciar foto de " + nome, 15),
//                         color: style.secondaryColor(),
//                         onPressed: (() {
//                           final formReport = _keyReport.currentState;
//                           if (formReport != null) {
//                             if (formReport.validate()) {
//                               formReport.save();
//                               Navigator.pop(context, "");
//                               showConfirmReport();
//                               setState(() {
//                                 dataFeed["Reported"] = true;
//                               });
//                             }
//                           } else if (reportByAnyElse == false) {
//                             Navigator.pop(context, "");
//                             showConfirmReport();
//                             setState(() {
//                               dataFeed["Reported"] = true;
//                             });
//                           } else {
//                             return false;
//                           }
//                         }),
//                       ))
//                   : Padding(
//                       padding: EdgeInsets.only(left: 40, top: 10),
//                       child: style.textWhiteBold(
//                           "Você já denunciou essa publicação", 15))
//             ],
//           );
//   }

//   showConfirmReport() {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return SimpleDialog(
//             backgroundColor: style.primaryColorTransparent(),
//             title: style.textWhiteBold("Denúncia enviada!", 20),
//             children: <Widget>[
//               Padding(
//                 padding: EdgeInsets.only(left: 20, right: 10),
//                 child: style.textWhiteBold(
//                     "Sua denúncia foi enviada com sucesso, iremos avaliá-la e, caso tenha algo de errado, tomaremos alguma providência!\nObrigado por sua contribuição!",
//                     15),
//               ),
//               Padding(
//                   padding: EdgeInsets.only(right: 115, left: 115, top: 10),
//                   child: Container(
//                       decoration: BoxDecoration(
//                           color: style.primaryColorSolid(),
//                           borderRadius: BorderRadius.all(Radius.circular(30)),
//                           border: Border.all(
//                               width: 1, color: style.secondaryColor())),
//                       child: IconButton(
//                         icon: Icon(Icons.close, size: 25),
//                         color: style.secondaryColor(),
//                         onPressed: (() {
//                           Navigator.pop(context, "");
//                         }),
//                       )))
//             ],
//           );
//         });
//   }

//   Widget reportItems(String nome, bool value, dataFeed) {
//     return Row(
//       children: <Widget>[
//         Theme(
//             data: Theme.of(context).copyWith(accentColor: style.white()),
//             child: Checkbox(
//               checkColor: style.primaryColorSolid(),
//               activeColor: style.secondaryColor(),
//               value: value,
//               onChanged: (bool value) {
//                 setState(() {
//                   switch (nome) {
//                     case "explicito":
//                       if (value == true) {
//                         this.reportByExplict = value;
//                         this.reportByActivitie = !value;
//                         this.reportByAnyElse = false;
//                         this.reportCase = "Conteúdo explícito";
//                       }
//                       break;
//                     case "atividade":
//                       if (value == true) {
//                         this.reportByExplict = !value;
//                         this.reportByActivitie = value;
//                         this.reportByAnyElse = !value;
//                         this.reportCase = "Atividade inválida";
//                       }
//                       break;
//                     case "outro":
//                       if (value == true) {
//                         reportByExplict = !value;
//                         reportByActivitie = !value;
//                         reportByAnyElse = value;
//                       }
//                   }
//                 });
//                 reportPopUp(dataFeed);
//               },
//             ))
//       ],
//     );
//   }
