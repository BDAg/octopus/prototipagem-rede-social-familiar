import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:befamily/styles.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
// import 'package:intl/intl.dart';

class TelaComentarios extends StatefulWidget{
  final Map dataFeed;
  TelaComentarios(this.dataFeed);
  @override
  _TelaComentariosState createState() => _TelaComentariosState(this.dataFeed);
}

class _TelaComentariosState extends State<TelaComentarios> {
  Widget _telaComentarios;

  final Styles style = Styles();
  List<CommentGen> _comments= <CommentGen>[];

  final GlobalKey<FormState> _keyComment = new GlobalKey<FormState>();
  final Map dataFeed;
  UsuarioBloc usuarioBloc;
  _TelaComentariosState(this.dataFeed);

  @override
  Widget build(BuildContext context) {
    if(_telaComentarios == null){
      _telaComentarios = createScreen(context);
    }
    return _telaComentarios;
  }
  Widget createScreen(BuildContext context){
    this.usuarioBloc = BlocProvider.of<UsuarioBloc>(context);

    return Scaffold(
      backgroundColor: style.secondaryColor(),
      appBar: AppBar(
        title: Text("Comentários"),
        backgroundColor: style.primaryColorSolid(),
      ),
      body: SafeArea(
        child: 
        Column(
          children: <Widget>[ 
            Expanded(
              child: FutureBuilder(
                future: usuarioBloc.listCommentaries(usuarioBloc.token, dataFeed["_id"]),
                initialData: [],
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.none:
                      return Container(
                        alignment: Alignment.center,
                        width: 200.0,
                        height: 200.0,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                          strokeWidth: 5.0,
                        ),
                      );
                    default:
                      if (snapshot.hasError) return Container();
                      else {
                        List data = snapshot.data;
                        _comments = <CommentGen>[];
                        for(int j = 0; j< data.length; j++){
                          _comments.insert(j, new CommentGen(data: snapshot.data[j]));
                        }
                        return ListView.builder(
                          reverse: true,
                          padding: EdgeInsets.all(10.0),
                          itemCount: _comments.length,
                          itemBuilder: (context, index) => _comments[index]
                        );
                      }
                  };
                }
              ),
            ),
            Container(
              child: form(context)
            ),
          ]
        )
      ),      
    );
  }

  Widget form(context){
    final FocusNode focusButton = new FocusNode();
    double width = MediaQuery.of(context).size.width;
    String comentario;
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
          height: 40,
          width: width-40,
          child: new Form(
          key: _keyComment,
          child:
            new TextFormField(
              style: new TextStyle(color: style.white(), fontSize: 14, fontWeight: FontWeight.bold),
              focusNode: focusButton,
              validator: (dat){
                if(dat.length<1){
                  return("Escreva um comentário");
                }
              },
              decoration: style.inputDecorator("", null),
              onSaved: (val) => comentario = val,
            )
          )
        ),
        new Padding(
          padding:
            new EdgeInsets.only(bottom: 3),
          child:
          new Container(
            height: 40,
            width: 20,
            child: new IconButton(icon: style.iconGen(Icons.send, 18),
            onPressed: ((){
              final form = _keyComment.currentState;
              if(form.validate()){
                form.save();
                form.reset();
                comentar(usuarioBloc.token, dataFeed["_id"], comentario);
                SystemChannels.textInput.invokeMethod('TextInput.hide');
              }else{
                print("Formulário inválido");
              }
              // showComments(comments, dataFeed, isNavigator);
            }),),
          )
        )
      ]
    );
  }

  Text getTime(int tempo){
      if(tempo<0)
        tempo *=-1;
      if((tempo~/86400)>=1 && (tempo~/86400)<30){
        return style.textWhite((tempo~/86400).toString()+"d", 13);
      }else
      if((tempo~/3600)>=1 && (tempo~/3600)<24){
        return style.textWhite((tempo~/3600).toString()+"h", 13);
      }else
      if((tempo~/60)>=1 && (tempo~/60)<60){
        return style.textWhite((tempo~/60).toString()+"m", 13);
      }else{
        return style.textWhite(tempo.toString()+"s", 13);
      }
  }

  void comentar(String token, String idMidia, String comment) async{
    final response = await usuarioBloc.postCommentary(token, idMidia, comment);
    if (response != null) {
      print(response);
      setState(() {
        _telaComentarios = null;
      });
    } else {
      print("Deu horrível!");
    }
  }
}

class CommentGen extends StatelessWidget{
    CommentGen({this.data});
    final Styles style = new Styles();
    final data;

    @override
    Widget build(BuildContext context){

    // var dataPublicacao = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(data["dateTime"].replaceAll("T", " "));
    
    final dataPublicacao = DateTime.parse(data['dateTime']).toLocal();
    var now = DateTime.now().toUtc().toLocal();
    int tempoPub = dataPublicacao.difference(now).inSeconds;
    Text textTime = getTime(tempoPub);
    return new Container(
      padding: new EdgeInsets.only(bottom: 5, top: 3),
      decoration: new BoxDecoration(
        border: new Border(
          bottom: new BorderSide(width: .9, color: Color.fromRGBO(255, 255, 255, .1))
        )
      ),
      child: new 
      Column(
        children: <Widget>[
        new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(right: 12),
              decoration:
                new BoxDecoration(border: Border.all(width: 1, color: style.white()), borderRadius: new BorderRadius.all(Radius.circular(25))),
                child:
                new ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  child: data["foto"] == '' ?
                    new Icon(Icons.person, color: Colors.white) : new CachedNetworkImage(
                      imageUrl: "http://35.231.220.66:4000/images/"+data["foto"],
                      placeholder: (context, url) => new CircularProgressIndicator(),
                      errorWidget: (context, url, error) => new Icon(Icons.info, color: Colors.white),
                      fit: BoxFit.fitWidth,width: 30, height: 30
                    ),
                )
              ),

            new Flexible(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                // new Row(children: <Widget>[
                  style.paddingRight(8),
                  style.textWhiteBold(data["nome"], 17),
                  style.paddingLeft(5),
                  Container(
                    margin: EdgeInsets.only(right: 20.0),
                    child:
                    style.textWhite(data["comentario"], 17)
                  )
                // ],)
              ]),
            ),
          ]
        ),
        new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  padding: new EdgeInsets.only(left: 50),
                ),
                textTime
              ],
            )
          ],
        )
      ])
    );
  }
    Text getTime(int tempo){
      if(tempo<0)
        tempo *=-1;
      if((tempo~/86400)>=1 && (tempo~/86400)<30){
        return style.textWhite((tempo~/86400).toString()+"d", 13);
      }else
      if((tempo~/3600)>=1 && (tempo~/3600)<24){
        return style.textWhite((tempo~/3600).toString()+"h", 13);
      }else
      if((tempo~/60)>=1 && (tempo~/60)<60){
        return style.textWhite((tempo~/60).toString()+"m", 13);
      }else{
        return style.textWhite("agora", 13);
      }
  }
}