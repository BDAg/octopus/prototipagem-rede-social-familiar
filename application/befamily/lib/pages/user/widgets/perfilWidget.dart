import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class PerfilWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    final blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    final pathFoto = blocUsuario.userData.fotoPerfil;

    final fotoUrl = 'http://35.231.220.66:4000/images/' + pathFoto;

    Widget imgUser(path) {
      if (path != '') {
        return Image.network(fotoUrl,
            width: 100.0, height: 100.0, fit: BoxFit.cover);
      } else {
        return Icon(
          Icons.person,
          size: 100.0,
          color: Colors.white,
        );
      }
    }

    return Container(
        padding: EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  child: imgUser(pathFoto),
                  padding: EdgeInsets.only(right: 20.0),
                ),
                Flexible(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                            '${blocUsuario.userData.nome} ${blocUsuario.userData.sobrenome}',
                            style:
                                TextStyle(color: Colors.white, fontSize: 27.0)),
                        Text(
                          '${blocUsuario.userData.descricao}',
                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                          softWrap: true,
                          //textAlign: TextAlign.justify,
                          overflow: TextOverflow.clip,
                        )
                      ],
                    ))
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed('/emocoes');
                    },
                    child: Column(
                      children: <Widget>[
                        StreamBuilder(
                          stream: blocUsuario.moments,
                          initialData: 0,
                          builder: (context, snapshot) {
                            return Text('${snapshot.data}',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 27.0));
                          },
                        ),
                        Text('Momentos',
                            style:
                                TextStyle(color: Colors.white, fontSize: 17.0)),
                      ],
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 20.0),
                      child: SizedBox(
                        width: 1.0,
                        height: 30.0,
                        child: Container(color: Colors.white),
                      )),
                  GestureDetector(
                    //tem que fazer isso mas n sei pq ta dando erro
                    onTap: () {
                      Navigator.of(context).pushNamed('/family-list');
                    },
                    child: Column(
                      children: <Widget>[
                        //tem que mudar o 2 para o tanto de familias que você está dentro
                        Text('${blocFamilia.families.length}',
                            style:
                                TextStyle(color: Colors.white, fontSize: 27.0)),
                        Text('Famílias',
                            style:
                                TextStyle(color: Colors.white, fontSize: 17.0)),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 1.0,
                child: Container(color: Colors.white),
              ),
            )
          ],
        ));
  }
}
