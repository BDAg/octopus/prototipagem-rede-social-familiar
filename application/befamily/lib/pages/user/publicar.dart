import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/blocs/familiaBloc.dart';

import 'package:befamily/styles.dart';
import 'package:image_picker/image_picker.dart';

import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Publicar extends StatefulWidget {
  Map<String, dynamic> data;
  Publicar({this.data});

  @override
  _PublicarState createState() => new _PublicarState(data: this.data);
}

class _PublicarState extends State<Publicar> {
  
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormPessoal = GlobalKey<FormState>();
  final style = Styles();

  bool fotoCarregada = true;
  bool _saving = false;
  bool _disableButton = false;
  
  String base64Image;

  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;

  Map<String, dynamic> data;

  _PublicarState({this.data});

  @override
  void initState() { 
    super.initState();
    this.base64Image = base64Encode(data['fotoPerfil'].readAsBytesSync());
    data['legenda'] = '';
  }

  Widget _imagem() {
    if (data['fotoPerfil'] != null) {
      return Image.file(data['fotoPerfil'], height: 200, width: 200, fit: BoxFit.fitWidth,); 
    }
    else {
      return Icon(Icons.person_add, size: 200, color: style.white());
    }
  }

  @override
  Widget build(BuildContext context) {

    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Scaffold(
              key: _keyScaffold,
              backgroundColor: style.secondaryColor(),
              body: ModalProgressHUD(
                color: style.secondaryColor(),
                progressIndicator: CircularProgressIndicator(backgroundColor: style.primaryColorSolid(), valueColor: AlwaysStoppedAnimation(style.secondaryColor())),
                inAsyncCall: _saving,
                child: ListView(
                  padding: EdgeInsets.all(20.0),
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Form(
                          key: _keyFormPessoal,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: style.textFormPadding(),
                                child:
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: _mostrarPopUp,
                                      child: Container(
                                        child: _imagem(),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 1,
                                            color: style.white()
                                          )
                                        )
                                      )
                                    ),
                                ]),
                              ),
                              Padding(
                                padding: style.textFormPadding(),
                                child: TextFormField(
                                  decoration: style.inputDecorator("Legenda", Icon(Icons.question_answer, color: style.white(),)),
                                  maxLines: 4,
                                  keyboardType: TextInputType.multiline,
                                  cursorWidth: 1,
                                  cursorColor: style.white(),
                                  onSaved: (val) => data['legenda'] = val,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.white
                                  ),
                                ),
                              ),
                            ],
                          )
                        ),
                        ButtonTheme(
                          minWidth: 200.0,
                          height: 40.0,
                          child: RaisedButton.icon(
                            color: style.primaryColorSolid(),
                            onPressed: _disableButton ? null : _publicar,
                            icon: Icon(
                              Icons.done,
                              color: Colors.black,
                            ),
                            label: Text(
                              'Publicar',
                              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24),
                            ),
                          )
                        ),
                      ],
                    )
                  ],
                ),
              )
            )
          ]
        )
      )
    );
  }

  void _success() {
    setState(() {
      _saving = false;
      _disableButton = true;
    });
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(
      SnackBar(
        content: Text('Publicado com sucesso!', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: style.primaryColorSolid(),
        duration: Duration(seconds: 3),
      )
    );
    Future.delayed(Duration(seconds: 3), () { blocUsuario.setIndex(0); Navigator.of(context).popUntil(ModalRoute.withName('/home')); });
  }

  void _fail() {
    setState(() {
      _saving = false;
    });
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('Falha ao publicar!', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
      backgroundColor: style.primaryColorSolid(),
      duration: Duration(seconds: 4),
    ));
  }

  void _publicar() async {
    if (_keyFormPessoal.currentState.validate()) {
      _keyFormPessoal.currentState.save();
      final name = DateTime.now().millisecond.toString();
      final token = blocUsuario.token;
      final families = blocFamilia.families.map( (dados) { return dados['_id']; } ).toList();
      if (fotoCarregada) {
        setState(() {
          _saving = true;
        });
        final newImage = await FlutterNativeImage.compressImage(data['fotoPerfil'].path, quality: 40);
        base64Image = base64Encode(newImage.readAsBytesSync());
        blocUsuario.uploadMidia(name, base64Image, token, token).then(
          (dados) {
            blocUsuario.publicar(token, this.data['tipoAtividade'], this.data['legenda'], families, dados, _success, _fail);
          }
        );
      } else {
        FocusScope.of(context).requestFocus(new FocusNode());
        _keyScaffold.currentState.showSnackBar(
          SnackBar(
            content: Text('Selecione uma Imagem!', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
            backgroundColor: style.primaryColorSolid(),
            duration: Duration(seconds: 3),
          )
        );
      }
    } else {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text('Verifique os dados!', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          backgroundColor: style.primaryColorSolid(),
          duration: Duration(seconds: 3),
        )
      );
    }
  }

  void _mostrarPopUp() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          backgroundColor: Colors.transparent,
          children: <Widget>[
            RaisedButton.icon(
              color: style.secondaryColor(),
              icon: Icon(Icons.photo_album, color: style.white(),),
              onPressed: _escolherGaleria,
              label: Text('Escolher da galeria', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            RaisedButton.icon(
              color: style.secondaryColor(),
              icon: Icon(Icons.photo_camera, color: style.white(),),
              onPressed: _escolherCamera,
              label: Text('Tirar Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            data['fotoPerfil'] == null
            ? Center(child:Text('Nenhuma imagem selecionada', style: TextStyle(fontWeight:FontWeight.bold, color: style.white()),))
            :
            Column(
              children: <Widget> 
              [
                Image.file(data['fotoPerfil'], height: 200, width: 200,),
                fotoCarregada == true ?
                RaisedButton.icon(
                  color: style.secondaryColor(),
                  icon: Icon(Icons.photo_camera, color: style.white(),),
                  onPressed: _excluir,
                  label: Text('Excluir Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                  ),
                )                       : 
                RaisedButton.icon(
                  color: style.secondaryColor(),
                  icon: Icon(Icons.photo_camera, color: style.white(),),
                  onPressed: _carregar,
                  label: Text('Carregar Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24),
                  ),
                )
              ]
            )
          ],
        );
      }
    );
  }

  void _escolherCamera () async {
    data['fotoPerfil'] = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context, "OK");
    _mostrarPopUp();
  }

  void _escolherGaleria() async {
    data['fotoPerfil'] = await ImagePicker.pickImage(source: ImageSource.gallery);

    Navigator.pop(context, "OK");
    _mostrarPopUp();
  }

  void _carregar() {
    base64Image = base64Encode(data['fotoPerfil'].readAsBytesSync());
    Navigator.pop(context, "OK");
    setState(() {
      data['fotoPerfil'] = data['fotoPerfil'];
      fotoCarregada = true;
    });
  }
  
  void _excluir(){
    Navigator.pop(context, "OK");
    setState(() {
      data['fotoPerfil'] = null;
      fotoCarregada = false;
    });
  }

}
