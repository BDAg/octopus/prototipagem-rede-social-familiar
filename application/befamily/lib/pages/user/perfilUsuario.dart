import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/pages/user/widgets/perfilWidget.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

import 'package:transparent_image/transparent_image.dart';

class PerfilUsuario extends StatefulWidget {
  @override
  _PerfilUsuarioState createState() => new _PerfilUsuarioState();
}

class _PerfilUsuarioState extends State<PerfilUsuario> {
  final style = Styles();
  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;
  int qtdMoments;

  @override
  Widget build(BuildContext context) {
    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);
    
    void _successLogout() async {
      await blocFamilia.logout();
      Navigator.of(context).pushNamed('/login');
    }

    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
              backgroundColor: Colors.transparent,
              body: Container(
                  padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 10.0),
                  child: Column(children: <Widget>[
                    Stack(
                      children: <Widget>[
                        PerfilWidget(),
                        Align(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            icon: Icon(Icons.exit_to_app, color: Colors.white),
                            onPressed: () {
                              blocUsuario.logout(_successLogout);
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: IconButton(
                            icon: Icon(Icons.build, color: Colors.white),
                            onPressed: () {
                              // Navigator.of(context)
                              //     .pushNamed('/perfil-familia');
                              //rota ficticia para fazer link com as config
                            },
                          ),
                        ),
                      ],
                    ),
                    FutureBuilder(
                      future: this.blocUsuario.getMideasUser(blocUsuario.token,
                          (data) {
                        blocUsuario.setMoments(data.length);
                      }),
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.waiting:
                          case ConnectionState.none:
                            return Container(
                              alignment: Alignment.center,
                              width: 200.0,
                              height: 200.0,
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                                strokeWidth: 5.0,
                              ),
                            );
                          default:
                            if (snapshot.hasError)
                              return Container();
                            else {
                              return Expanded(
                                flex: 10,
                                child: GridView.builder(
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 3,
                                          crossAxisSpacing: 3.0,
                                          mainAxisSpacing: 3.0),
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      child: FadeInImage.memoryNetwork(
                                        image: "http://35.231.220.66:4000/images/" + snapshot.data[index]['path'],
                                        fit: BoxFit.cover,
                                        placeholder: kTransparentImage,
                                      ),
                                      // child: Image.network(
                                      //     "http://35.231.220.66:4000/images/" +
                                      //         snapshot.data[index]['path'],
                                      //     fit: BoxFit.cover),
                                      onTap: () {
                                        print(snapshot.data[index]);
                                      },
                                    );
                                  },
                                ),
                              );
                            }
                        }
                      },
                    )
                  ])))
        ])));
  }
}
