import 'package:flutter/material.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/blocs/familiaBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:befamily/styles.dart';
import 'package:befamily/pages/user/widgets/feedPublicacoes.dart';

class Feed extends StatefulWidget{
  @override
  _FeedState createState() => new _FeedState();
}

class _FeedState extends State<Feed> with SingleTickerProviderStateMixin {
  
  final _commentsGenController = new ScrollController();
  final style = Styles();
  final keyScaffold = GlobalKey<ScaffoldState>();

  bool iconLike;
  bool reportByExplict;
  bool reportByActivitie;
  bool reportByAnyElse;

  String reportCase;

  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;

  List families;

  @override
  Widget build(BuildContext context) {

    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);
    this.families = blocFamilia.families.map( (dados) { return dados['_id']; } ).toList();

    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      key: keyScaffold,
      body: 
      SafeArea(
        minimum: EdgeInsets.only(top: 20.0),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Scaffold(
              backgroundColor: style.secondaryColor(),
              body: StreamBuilder(
                stream: blocUsuario.updateFeed,
                builder: (context, snapshot) {
                  return FutureBuilder(
                    future: blocUsuario.getMideas(blocUsuario.token, families),
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                        case ConnectionState.none:
                          return Center(
                            child: Container(
                              alignment: Alignment.center,
                              width: 200.0,
                              height: 200.0,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                                strokeWidth: 5.0,
                              ),
                            ),
                          );
                        default:
                          if (snapshot.hasError) return Container(
                            child: Center(
                              child: Text(
                                "Ocorreu um erro! Por favor, tente mais tarde!",
                                style: TextStyle(color: Colors.white, fontSize: 20.0),
                                softWrap: true,
                                overflow: TextOverflow.clip,
                              ),
                            ),
                          );
                          else {
                            if (snapshot.hasData) {
                              if (snapshot.data.length > 0) {
                                blocUsuario.setTimestamp(DateTime.parse(snapshot.data[0]['dateTime']).millisecondsSinceEpoch);
                                return ListView.builder(
                                  padding: EdgeInsets.all(10.0),
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (context, index){
                                    return new FeedPublicacoesWidget(dataFeed: snapshot.data[index], userID: blocUsuario.userData.idUser, keyScaffold: keyScaffold,);
                                  },
                                );
                              } else {
                                return Container(
                                  child: Center(
                                    child: style.textWhiteBold("Sem publicações!", 30.0),
                                  ),
                                );
                              }
                            } else {
                              return Container(
                                child: Center(
                                  child: style.textWhiteBold("Sem publicações!", 30.0),
                                ),
                              );
                            }
                          }
                      }
                    },
                  );
                },
              )
            )
          ]
        )
      )
    );
  }
}
