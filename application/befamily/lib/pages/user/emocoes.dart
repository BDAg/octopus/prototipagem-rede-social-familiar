import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/pages/sizeConfig.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Emocoes extends StatefulWidget {
  @override
  _EmocoesState createState() => new _EmocoesState();
}

class _EmocoesState extends State<Emocoes> {
  final style = Styles();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    final blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Scaffold(
                backgroundColor: style.secondaryColor(),
                body: Container(
                  padding: EdgeInsets.fromLTRB(10.0, 25.0, 10.0, 10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 2,
                            child: Text(
                              'Bem vindo ${blocUsuario.userData.nome}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: SizeConfig.blockSizeHorizontal * 7
                              ),
                              softWrap: true,
                              //textAlign: TextAlign.justify,
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              flex: 2,
                              child: Text(
                                'Como está se sentindo hoje?',
                                style: TextStyle(
                                  color: style.primaryColorSolid(),
                                  fontSize: SizeConfig.blockSizeHorizontal * 6
                                ),
                                softWrap: true,
                                //textAlign: TextAlign.justify,
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        )
                      ),
                      Expanded(
                        child: ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                                Navigator.of(context).pushNamed('/home');
                              },
                              child: Padding(
                                padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.sentiment_satisfied,
                                      color: Colors.white,
                                      size: SizeConfig.blockSizeHorizontal * 12,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                      child: Text(
                                        'Feliz',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: SizeConfig.blockSizeHorizontal * 7
                                        )
                                      ),
                                    ),
                                  ],
                                )
                              )
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushNamed('/home');
                              },
                              child: Padding(
                                padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.sentiment_very_satisfied,
                                      color: Colors.white,
                                      size: SizeConfig.blockSizeHorizontal * 12,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                      child: Text('Curtindo',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: SizeConfig.blockSizeHorizontal * 7
                                            )
                                          ),
                                    ),
                                  ],
                                )
                              )
                            ),
                            GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.of(context).pushNamed('/home');
                                },
                                child: Padding(
                                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          MdiIcons.heart,
                                          color: Colors.white,
                                          size: SizeConfig.blockSizeHorizontal * 12,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                          child: Text('Amando',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: SizeConfig.blockSizeHorizontal * 7)),
                                        ),
                                      ],
                                    ))),
                                                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/home');
                                },
                                child: Padding(
                                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.tag_faces,
                                          color: Colors.white,
                                          size: SizeConfig.blockSizeHorizontal * 12,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                          child: Text('De boa',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: SizeConfig.blockSizeHorizontal * 7)),
                                        ),
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/home');
                                },
                                child: Padding(
                                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.sentiment_neutral,
                                          color: Colors.white,
                                          size: SizeConfig.blockSizeHorizontal * 12,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                          child: Text('Desapontado(a)',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: SizeConfig.blockSizeHorizontal * 7)),
                                        ),
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/home');
                                },
                                child: Padding(
                                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.sentiment_very_dissatisfied,
                                          color: Colors.white,
                                          size: SizeConfig.blockSizeHorizontal * 12,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                          child: Text('Irritado(a)',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: SizeConfig.blockSizeHorizontal * 7)),
                                        ),
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/home');
                                },
                                child: Padding(
                                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.sentiment_dissatisfied,
                                          color: Colors.white,
                                          size: SizeConfig.blockSizeHorizontal * 12,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                          child: Text('Triste',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: SizeConfig.blockSizeHorizontal * 7)),
                                        ),
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/home');
                                },
                                child: Padding(
                                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.mood_bad,
                                          color: Colors.white,
                                          size: SizeConfig.blockSizeHorizontal * 12,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                                          child: Text('Depressivo(a)',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: SizeConfig.blockSizeHorizontal * 7)),
                                        ),
                                      ],
                                    ))),
                          ],
                        )
                      )
                    ],
                  )))
        ])));
  }
}
