import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/blocs/familiaBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';

import 'package:image_picker/image_picker.dart';
import 'package:befamily/styles.dart';
import 'package:befamily/models/familia.dart';

import 'package:flutter_native_image/flutter_native_image.dart';

import 'package:modal_progress_hud/modal_progress_hud.dart';

class CriarFamilia extends StatefulWidget {
  @override
  _CriarFamiliaState createState() => new _CriarFamiliaState();
}

class _CriarFamiliaState extends State<CriarFamilia> {
  
  final _keyScaffold = new GlobalKey<ScaffoldState>();
  final _keyFormFamilia = GlobalKey<FormState>();
  final familiaModel = FamiliaModel();
  final style = new Styles();

  File fotoFamilia;

  String _codigo;
  String fotoFamiliaString;

  int privacidadeAtual;

  final familiaDicionario = {};

  bool privadoPrivacidade;
  bool publicoPrivacidade;
  bool fotoCarregada;
  bool familiaCriada = false;
  bool _saving = false;
  bool _disableButton = false;

  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;

  @override
  void initState() {
    super.initState();
    fotoCarregada = false;
    fotoFamilia = null;
    privadoPrivacidade = false;
    publicoPrivacidade = true;
    privacidadeAtual = 0;
    _codigo = null;
  }

  @override
  Widget build(BuildContext context) {

    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    return Scaffold(
      key: _keyScaffold,
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Scaffold(
              backgroundColor: style.secondaryColor(),
              body: ModalProgressHUD(
                color: style.secondaryColor(),
                progressIndicator: CircularProgressIndicator(backgroundColor: style.primaryColorSolid(), valueColor: AlwaysStoppedAnimation(style.secondaryColor())),
                inAsyncCall: _saving,
                child: ListView(
                  padding: EdgeInsets.all(10.0),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 15.0),
                      child: Center(
                        child: formCriarFamilia(context)
                      )
                    )
                  ],
                ),
              )
            ),
          ],
        ),
      ),
    );
  }

  Widget formCriarFamilia(BuildContext context) {
    return new Column(
      children: <Widget>[
        style.textWhiteBold("Escolher foto da família", 18),
        Form(
          key: _keyFormFamilia,
          child: Column(
            children: <Widget>[
              Padding(
                padding: style.textFormPadding(),
                child:
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      child: Container(child: _imagem(), decoration: BoxDecoration(border: Border.all(width: 1, color: style.white())),),
                      onTap: _mostrarPopUp,
                    ),
                ]),
              ),
              _textFormFieldGenerator("Nome da Família", "Nome da família é obrigatório", "nome", null, null, 1, Icon(Icons.group, color: style.white(),)),
              _textFormFieldGenerator("Descrição", "Descrição é obrigatório", "descricao", 300, TextInputType.multiline, 4, Icon(Icons.description, color: style.white(),)),
              Container(
                decoration: ShapeDecoration(shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(24)),
                  side: new BorderSide(width: 1, color: style.white())
                )),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left: 11, right: 11),
                              child:
                              Icon(Icons.visibility, color: style.white(), size: 25,),
                            ),
                            style.textWhiteBold("Privacidade", 18),
                          ]
                        )
                      ]
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: privacidade("Privado", privadoPrivacidade),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 15, left: 15),
                      child: privacidade("Público", publicoPrivacidade)
                    )
                  ]
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: ButtonTheme(
                  minWidth: 200.0,
                  height: 30.0,
                  child: RaisedButton(
                    color: style.primaryColorSolid(),
                    onPressed: _disableButton ? null : _criarFamilia,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                    ),
                    padding: EdgeInsets.all(12.0),
                    child: Text('Criar Família', style: TextStyle(color: Colors.black, fontSize: 20.0)),
                  )
                ),
              ),
              _codigo == null ? Text("") : 
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    style.textWhiteBold("O código da sua família:", 18),
                    PhysicalModel(
                      shadowColor: style.primaryColorSolid(),
                      elevation: 15,
                      color: style.secondaryColor(),
                      child:
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child:
                        GestureDetector(
                          child: style.textWhiteBold(_codigo, 18),
                          onTap: (){
                            Clipboard.setData(new ClipboardData(text: _codigo));
                            _keyScaffold.currentState.showSnackBar(
                              new SnackBar(content: Text("Código copiado", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)), backgroundColor: style.primaryColorSolid()),
                            );
                          },
                        )
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 25.0),
                      child: ButtonTheme(
                        minWidth: 200.0,
                        height: 30.0,
                        child: RaisedButton.icon(
                          color: style.primaryColorSolid(),
                          onPressed: () async {
                            final _families = await blocUsuario.findFamiliesUser();
                            await blocFamilia.setFamilies(_families);
                            Navigator.of(context).popAndPushNamed('/home');
                          },
                          icon: Icon(
                            Icons.done,
                            color: Colors.black,
                          ),
                          label: Text(
                            'Avançar',
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          ),
                        )
                      )
                    ),
                  ],
                ),
              )
            ]
          )
        )
      ],
    );
  }
  void _criarFamilia() {
    final formFamilia = _keyFormFamilia.currentState;
    if(formFamilia.validate() && familiaCriada == false) {
      formFamilia.save();
      familiaModel.nome = familiaDicionario['nome'];
      familiaModel.descricao = familiaDicionario['descricao'];
      familiaModel.privacidade = privacidadeAtual;
      blocUsuario.createFamily(familiaModel.toJsonApi()).then(
        (dados) async {
          if (fotoCarregada) {
            setState(() {
              _saving = true;
            });
            final newImage = await FlutterNativeImage.compressImage(fotoFamilia.path, quality: 40);
            final base64Image = base64Encode(newImage.readAsBytesSync());
            blocUsuario.uploadMidia("fotoPerfil", base64Image, blocUsuario.token, dados['token']).then(
              (dadosMidia) async { 
                await blocFamilia.updateFotoPerfil(blocUsuario.token, dadosMidia['path'], dados['idFamily'], _success, _fail);
                setState(() {
                  _codigo = dados['idFamily'];
                  familiaCriada = true;
                });
              }
            );
          } else {
            _success();
            setState(() {
              _codigo = dados['idFamily'];
              familiaCriada = true;
            });
          }
        }
      ).catchError(
        (err) {
          print(err);
          _fail();
        }
      );
    }
  }

  void _success() {
    setState(() {
      _saving = false;
    });
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(
      SnackBar(
        content: Text('Cadastrado com sucesso!', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: style.primaryColorSolid(),
        duration: Duration(seconds: 3),
      )
    );
  }

  void _fail() {
    setState(() {
      _saving = false;
    });
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('Falha ao cadastrar!', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
      backgroundColor: style.primaryColorSolid(),
      duration: Duration(seconds: 4),
    ));
  }

  Widget _textFormFieldGenerator(String nomeCampo, String textoValidator, String index, int len, TextInputType keyboardType, int lines, Icon icone){
    return Padding(
      padding: style.textFormPadding(),
      child:
        TextFormField(
        decoration: style.inputDecorator(nomeCampo, icone),
        onSaved: (val) => familiaDicionario[index] = val,
        maxLines: lines,
        keyboardType: keyboardType,
        cursorWidth: 1,
        maxLength: len,
        cursorColor: style.white(),
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
          color: Colors.white
        ),
        validator: (data){
          if(data.isEmpty){
            return textoValidator;
          }
        },
      )
    );
  }

  Widget privacidade(String nome, bool value){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10, top: 5),
          child:
            style.textWhiteBold(nome, 18),
        ),
        Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: Colors.white,
          ),
          child: Checkbox(
          checkColor: style.primaryColorSolid(),
          activeColor: style.secondaryColor(),
          
          value: value,
          onChanged: (bool value) {
            setState(() {
              switch (nome) {
                case "Privado":
                  if(value == true){
                    privadoPrivacidade = value;
                    publicoPrivacidade = !value;
                    privacidadeAtual = 1;
                  }
                  break;
                case "Público":
                  if(value == true){
                    publicoPrivacidade = value;
                    privadoPrivacidade = !value;
                    privacidadeAtual = 0;
                  }
                  break;
              }
            });
          },
        )
        )
      ],
    );
  }
  
  void _mostrarPopUp() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          backgroundColor: Colors.transparent,
          children: <Widget>[
            RaisedButton.icon(
              color: style.primaryColorSolid(),
              icon: Icon(Icons.photo_album, color: style.white(),),
              onPressed: _escolherGaleria,
              label: Text('Escolher da galeria', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            RaisedButton.icon(
              color: style.primaryColorSolid(),
              icon: Icon(Icons.photo_camera, color: style.white(),),
              onPressed: _escolherCamera,
              label: Text('Tirar Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            fotoFamilia == null
            ? Center(child:Text('Nenhuma imagem selecionada', style: TextStyle(fontWeight:FontWeight.bold, color: style.white()),))
            :
            Column(
              children: <Widget>
              [
                Image.file(fotoFamilia, height: 200, width: 200,),
                fotoCarregada == true ?
                RaisedButton.icon(
                  color: style.secondaryColor(),
                  icon: Icon(Icons.photo_camera, color: style.white(),),
                  onPressed: _excluir,
                  label: Text('Excluir Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                  ),
                )                       : 
                RaisedButton.icon(
                  color: style.secondaryColor(),
                  icon: Icon(Icons.photo_camera, color: style.white(),),
                  onPressed: _carregar,
                  label: Text('Carregar Foto', style: TextStyle(color: style.white(), fontWeight:FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24),
                  ),
                )
              ]
            )
          ],
        );
      }
    );
  }
  
  Widget _imagem(){
    if (fotoFamilia != null){
      return Image.file(fotoFamilia, height: 100, width: 100, fit: BoxFit.fitWidth,); 
    }
    else
      return Icon(Icons.person_add, size: 100, color: style.white(),);
  }

  void _escolherCamera () async{
    fotoFamilia = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context, "OK");
    _mostrarPopUp();
  }

  void _escolherGaleria() async{
    fotoFamilia = await ImagePicker.pickImage(source: ImageSource.gallery);

    Navigator.pop(context, "OK");
    _mostrarPopUp();
  }

  void _carregar() {
    String base64Image = base64Encode(fotoFamilia.readAsBytesSync());
    Navigator.pop(context, "OK");
    setState(() {
      fotoFamiliaString = base64Image;
      fotoFamilia = fotoFamilia;
      fotoCarregada = true;
    });
  }

   void _excluir(){
    Navigator.pop(context, "OK");
    setState(() {
      fotoFamilia = null;
      fotoCarregada = false;
    });
  }
}