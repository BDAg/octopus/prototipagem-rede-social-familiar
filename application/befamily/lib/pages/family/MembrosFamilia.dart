import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/pages/family/widgets/memberItem.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class MembrosFamilia extends StatefulWidget {
  @override
  _MembrosFamiliaState createState() => _MembrosFamiliaState();
}

class _MembrosFamiliaState extends State<MembrosFamilia> {
  final style = Styles();

  Map<String, dynamic> data;

  FamiliaBloc blocFamilia;
  UsuarioBloc blocUsuario;

  @override
  Widget build(BuildContext context) {
    this.data = ModalRoute.of(context).settings.arguments;

    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
              backgroundColor: style.secondaryColor(),
              body: Column(children: <Widget>[
                Center(
                    heightFactor: 3,
                    widthFactor: 3,
                    child: Text('Membros da Sua familia',
                        style: TextStyle(fontSize: 30.0, color: Colors.white))),
                Expanded(
                  child: FutureBuilder(
                    future: blocFamilia.usersInfo(
                        blocUsuario.token, this.data['membros']),
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                        case ConnectionState.none:
                          return Container(
                            alignment: Alignment.center,
                            width: 200.0,
                            height: 200.0,
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white),
                              strokeWidth: 5.0,
                            ),
                          );
                        default:
                          if (snapshot.hasError)
                            return Container();
                          else {
                            return ListView.builder(
                                padding: EdgeInsets.all(10.0),
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, index) {
                                  return MemberItemWidget(
                                      data: snapshot.data[index]);
                                });
                          }
                      }
                    },
                  ),
                )
              ]))
        ])));
  }
}
