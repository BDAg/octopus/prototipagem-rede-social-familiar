import 'package:flutter/material.dart';

class MemberItemWidget extends StatelessWidget {
  Map<String, dynamic> data;

  MemberItemWidget({this.data});

  @override
  Widget build(BuildContext context) {

    imagem() {
      if (data['fotoPerfil'] == '') {
        return Icon(
          Icons.people,
          size: 60.0,
          color: Colors.white,
        );
      } else {
        return Image.network('http://35.231.220.66:4000/images/' + data['fotoPerfil'],
            width: 60.0, height: 60.0, fit: BoxFit.cover);
      }
    }

    return Padding(
      padding: EdgeInsets.only(bottom: 15.0),
      child: GestureDetector(
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: imagem(),
            ),
            Text(data['nome'],
                style: TextStyle(color: Colors.white, fontSize: 20.0))
          ],
        ),
        onTap: () {
          print('Você clicou na pessoa ' +
              data['nome'] +
              ' agora você vai visitar o perfil dela');
        },
      ),
    );
  }
}
