import 'package:flutter/material.dart';

import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';

class InfoPerfilWidget extends StatefulWidget {
  Map<String, dynamic> data;

  InfoPerfilWidget(this.data);

  @override
  _InfoPerfilWidgetState createState() => new _InfoPerfilWidgetState(this.data);
}

class _InfoPerfilWidgetState extends State<InfoPerfilWidget> {
  Map<String, dynamic> data;

  _InfoPerfilWidgetState(this.data);

  UsuarioBloc blocUsuario;

  @override
  Widget build(BuildContext context) {
    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    imagem() {
      if (data['foto'] == '') {
        return Icon(
          Icons.people,
          size: 70.0,
          color: Colors.white,
        );
      } else {
        return Image.network('http://35.231.220.66:4000/images/' + data['foto'],
            width: 100.0, height: 100.0, fit: BoxFit.cover);
      }
    }

    return Container(
        padding: EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  child: imagem(),
                  padding: EdgeInsets.only(right: 20.0),
                ),
                Flexible(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text('${data["nome"]}',
                            style:
                                TextStyle(color: Colors.white, fontSize: 27.0)),
                        Text(
                          '${data["descricao"]}',
                          style: TextStyle(color: Colors.white, fontSize: 17.0),
                          softWrap: true,
                          //textAlign: TextAlign.justify,
                          overflow: TextOverflow.clip,
                        )
                      ],
                    ))
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    StreamBuilder(
                      initialData: 0,
                      stream: blocUsuario.moments,
                      builder: (context, snapshot) {
                        return Text(
                          '${snapshot.data}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 27.0
                          )
                        );
                      },
                    ),
                    Text('Momentos',
                        style: TextStyle(color: Colors.white, fontSize: 17.0)),
                  ],
                ),
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                    child: SizedBox(
                      width: 1.0,
                      height: 50.0,
                      child: Container(color: Colors.white),
                    )),
                Column(
                  children: <Widget>[
                    GestureDetector(
                      child: Column(
                        children: <Widget>[
                          Text('${data["membros"].length}',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 27.0)),
                          Text('Membros',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 17.0)),
                        ],
                      ),
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed('/membros-familia', arguments: data);
                      },
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 1.0,
              child: Container(color: Colors.white),
            ),
          ],
        ));
  }
}
