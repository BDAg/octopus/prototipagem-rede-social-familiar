import 'package:befamily/pages/family/widgets/infoPerfil.dart';
import 'package:befamily/pages/user/configFamilia.dart';
import 'package:befamily/styles.dart';
import 'package:flutter/material.dart';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:befamily/blocs/usuarioBloc.dart';

class PerfilFamilia extends StatefulWidget {
  @override
  _PerfilFamiliaState createState() => new _PerfilFamiliaState();
}

class _PerfilFamiliaState extends State<PerfilFamilia> {
  Map<String, dynamic> data;
  
  final style = Styles();

  UsuarioBloc blocUsuario;

  @override
  Widget build(BuildContext context) {
    this.data = ModalRoute.of(context).settings.arguments;
    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    print(this.data);

    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
              backgroundColor: style.secondaryColor(),
              body: Container(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          InfoPerfilWidget(this.data),
                          Align(
                            alignment: Alignment.topRight,
                            child: IconButton(
                              icon: Icon(Icons.build, color: Colors.white),
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => ConfigFamilia(this.data)
                                  )
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      FutureBuilder(
                        future: this.blocUsuario.getMideasFamily(this.data['_id'], (data) { blocUsuario.setMoments(data.length); }),
                        builder: (context, snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                            case ConnectionState.none:
                              return Container(
                                alignment: Alignment.center,
                                width: 200.0,
                                height: 200.0,
                                child: CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation<Color>(Colors.white),
                                  strokeWidth: 5.0,
                                ),
                              );
                            default:
                              if (snapshot.hasError)
                                return Container();
                              else {
                                return Expanded(
                                  flex: 10,
                                  child: GridView.builder(
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 3,
                                            crossAxisSpacing: 3.0,
                                            mainAxisSpacing: 3.0),
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        child: Image.network( "http://35.231.220.66:4000/images/" +
                                            snapshot.data[index]['path'],
                                            fit: BoxFit.cover),
                                        onTap: () {
                                          print(snapshot.data[index]);
                                        },
                                      );
                                    },
                                  ),
                                );
                              }
                          }
                        },
                      )
                    ],
                  )))
        ])));
  }
}
