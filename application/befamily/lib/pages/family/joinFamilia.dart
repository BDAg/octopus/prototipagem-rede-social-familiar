import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class JoinFamilia extends StatefulWidget {
  @override
  _JoinFamiliaState createState() => new _JoinFamiliaState();
}

class _JoinFamiliaState extends State<JoinFamilia> {
  final codeFamily = TextEditingController();
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final style = Styles();

  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;

  @override
  Widget build(BuildContext context) {
    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: <Widget>[
          Scaffold(
              key: _keyScaffold,
              backgroundColor: style.secondaryColor(),
              body: SingleChildScrollView(
                  padding: EdgeInsets.only(top: 90.0, bottom: 40.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text('Entrar para Família?',
                              style: TextStyle(
                                  fontSize: 25.0, color: Colors.white)),
                        ),
                        Padding(
                            padding:
                                EdgeInsets.fromLTRB(40.0, 10.0, 40.0, 10.0),
                            child: TextField(
                              style: TextStyle(color: Colors.white),
                              controller: codeFamily,
                              decoration: style.inputDecorator(
                                  'Código de Acesso',
                                  Icon(Icons.person, color: Colors.white)),
                            )),
                        Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 10.0, 40.0, 60.0),
                          child: ButtonTheme(
                            minWidth: 200.0,
                            height: 10.0,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: joinFamily,
                              padding: EdgeInsets.all(12.0),
                              color: style.primaryColorSolid(),
                              child: Text('Entrar',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 20.0)),
                            ),
                          ),
                        ),
                        Center(
                          child: Text('Começar uma nova família?',
                              style: TextStyle(
                                  fontSize: 25.0, color: Colors.white)),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: ButtonTheme(
                            minWidth: 200.0,
                            height: 10.0,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: () {
                                Navigator.of(context)
                                    .popAndPushNamed('/criar-familia');
                              },
                              padding: EdgeInsets.all(12.0),
                              color: style.primaryColorSolid(),
                              child: Text('Criar',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 20.0)),
                            ),
                          ),
                        ),
                      ])))
        ])));
  }

  void joinFamily() async {
    if (codeFamily.text.replaceAll(' ', '') != '') {
      await blocFamilia.joinFamily(blocUsuario.token,
          codeFamily.text.replaceAll(' ', ''), _success, _fail);
    } else {
      FocusScope.of(context).requestFocus(new FocusNode());
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text('Verifique o código!',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: style.primaryColorSolid(),
        duration: Duration(seconds: 3),
      ));
    }
  }

  void _success() {
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('Entrou com sucesso!',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
      backgroundColor: style.primaryColorSolid(),
      duration: Duration(seconds: 3),
    ));
    Future.delayed(Duration(seconds: 3), () async {
      final _families = await blocUsuario.findFamiliesUser();
      await blocFamilia.setFamilies(_families);
      Navigator.of(context).popAndPushNamed('/home');
    });
  }

  void _fail() {
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text('Falha ao entrar! Verifique os dados!',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
      backgroundColor: style.primaryColorSolid(),
      duration: Duration(seconds: 4),
    ));
  }
}
