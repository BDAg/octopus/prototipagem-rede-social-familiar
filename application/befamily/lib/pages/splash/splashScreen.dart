import 'dart:async';

import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:befamily/styles.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Styles style = new Styles();

  @override
  void initState() {
    super.initState();
    startSplashScreenTimer();
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationToNextPage);
  }

  void navigationToNextPage() async {
    final blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    final blocFamilia = BlocProvider.of<FamiliaBloc>(context);

    if (blocUsuario.logged) {

      final _families = await blocUsuario.findFamiliesUser();
      await blocFamilia.setFamilies(_families);

      if (blocFamilia.families.length != 0) {
        if (blocUsuario.firstTime) {
          Navigator.of(context).pushNamed('/emocoes');
        } else {
          Navigator.of(context).pushNamed('/home');
        }
      } else {
        Navigator.of(context).pushNamed('/join-familia');
      }
    } else {
      Navigator.of(context).pushNamed('/login');
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget bemvindo = Padding(
      padding: EdgeInsets.all(4.0),
      child: Text(
        'Bem vindo',
        style: TextStyle(fontSize: 32.0, color: Colors.white),
      ),
    );

    Widget texto = Padding(
      padding: EdgeInsets.all(4.0),
      child: Text(
        'Familia unida é mais feliz!',
        style: TextStyle(fontSize: 24.0, color: Colors.white),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        minimum: EdgeInsets.only(top: 20.0),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("assets/img/familia1.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              body: ListView(
                padding: EdgeInsets.all(10.0),
                children: <Widget>[
                  Theme(
                    data: ThemeData(
                        brightness: Brightness.dark,
                        primarySwatch: Colors.teal),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 250.0),
                          child: Column(
                            children: <Widget>[bemvindo, texto],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
