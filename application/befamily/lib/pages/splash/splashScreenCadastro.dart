import 'package:befamily/styles.dart';
import 'package:flutter/material.dart';

class SplashScreenCadastro extends StatefulWidget {
  @override
  _SplashScreenCadastroState createState() => new _SplashScreenCadastroState();
}

class _SplashScreenCadastroState extends State<SplashScreenCadastro> {
  Styles style = new Styles();

  @override
  Widget build(BuildContext context) {
    Widget bemvindo = Padding(
      padding: EdgeInsets.all(4.0),
      child: Text(
        'Bem vindo',
        style: TextStyle(fontSize: 32.0, color: Colors.white),
      ),
    );

    Widget texto = Padding(
      padding: EdgeInsets.all(4.0),
      child: Text(
        'Familia unida é mais feliz!',
        style: TextStyle(fontSize: 24.0, color: Colors.white),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        minimum: EdgeInsets.only(top: 20.0),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("assets/img/familia.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              body: ListView(
                padding: EdgeInsets.all(10.0),
                children: <Widget>[
                  Form(
                      child: Theme(
                    data: ThemeData(
                        brightness: Brightness.dark,
                        primarySwatch: Colors.teal),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 250.0),
                          child: Column(
                            children: <Widget>[bemvindo, texto],
                          ),
                        )
                      ],
                    ),
                  )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
