import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:befamily/pages/user/familyList.dart';
import 'package:befamily/pages/user/feed.dart';
import 'package:befamily/pages/user/mapa.dart';
import 'package:befamily/pages/user/novaAtividade.dart';
import 'package:befamily/pages/user/perfilUsuario.dart';

import 'package:befamily/blocs/familiaBloc.dart';
import 'package:befamily/blocs/usuarioBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';

import 'package:befamily/styles.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final style = Styles();

  // final pageStream = StreamController<int>.broadcast();

  StreamController<int> pageStream;

  UsuarioBloc blocUsuario;
  FamiliaBloc blocFamilia;

  final _widgetOptions = [
    Feed(),
    FamilyList(),
    NovaAtividade(),
    // Mapa(),
    PerfilUsuario()
  ];

  @override
  Widget build(BuildContext context) {

    this.blocUsuario = BlocProvider.of<UsuarioBloc>(context);
    this.blocFamilia = BlocProvider.of<FamiliaBloc>(context);
    blocUsuario.verifyFeed(blocFamilia.families);

    this.pageStream = this.blocUsuario.pageStream;

    void _onItemTapped(int index) {
      pageStream.add(index);
    }

    Future<bool> _onWillPop() {
      return showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: new Text('Você tem certeza?'),
              content: new Text('Você quer sair do App'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: new Text('Não'),
                ),
                new FlatButton(
                  onPressed: () => SystemChannels.platform
                      .invokeMethod('SystemNavigator.pop'),
                  child: new Text('Sim'),
                ),
              ],
            ),
      ) ??
      false;
    }

    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
            key: _keyScaffold,
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: false,
            body: SafeArea(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Scaffold(
                      backgroundColor: style.secondaryColor(),
                      body: StreamBuilder<int>(
                        stream: pageStream.stream,
                        initialData: 0,
                        builder: (context, snapshot) {
                          return Center(
                            child: _widgetOptions.elementAt(snapshot.data),
                          );
                        },
                      ),
                      bottomNavigationBar: Theme(
                        data: Theme.of(context).copyWith(canvasColor: style.primaryColorSolid()),
                        child: StreamBuilder<int>(
                          stream: pageStream.stream,
                          initialData: 0,
                          builder: (context, snapshot) {
                            return BottomNavigationBar(
                              items: <BottomNavigationBarItem>[
                                BottomNavigationBarItem(
                                  icon: StreamBuilder(
                                    stream: blocUsuario.updateFeed,
                                    initialData: false,
                                    builder: (context, snapshot) {
                                      if (snapshot.data == false) {
                                        return Icon(
                                          Icons.home,
                                          color: style.secondaryColor()
                                        );
                                      } else {
                                        return Container(
                                          child: Stack(
                                            children: <Widget>[
                                              Icon(
                                                Icons.home,
                                                color: style.secondaryColor()
                                              ),
                                              Positioned(
                                                left: 10.0,
                                                child: Icon(
                                                  Icons.new_releases,
                                                  color: Colors.white,
                                                  size: 15.0
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                  title: Text(
                                    'Inicio',
                                    style: TextStyle(color: style.secondaryColor())
                                  )
                                ),
                                BottomNavigationBarItem(
                                    icon: Icon(Icons.people,
                                        color: style.secondaryColor()),
                                    title: Text('Familias',
                                        style:
                                            TextStyle(color: style.secondaryColor()))),
                                BottomNavigationBarItem(
                                    icon:
                                        Icon(Icons.add, color: style.secondaryColor()),
                                    title: Text('Momentos',
                                        style:
                                            TextStyle(color: style.secondaryColor()))),
                                // BottomNavigationBarItem(
                                //     icon: Icon(Icons.location_on,
                                //         color: style.secondaryColor()),
                                //     title: Text('Locais',
                                //         style:
                                //             TextStyle(color: style.secondaryColor()))),
                                BottomNavigationBarItem(
                                    icon: Icon(Icons.person,
                                        color: style.secondaryColor()),
                                    title: Text('Perfil',
                                        style:
                                            TextStyle(color: style.secondaryColor())))
                              ],
                              currentIndex: snapshot.data,
                              fixedColor: style.secondaryColor(),
                              onTap: _onItemTapped,
                            );
                          },
                        ),
                      )
                    )
                ]
              )
            )
          )
        );
  }
}
