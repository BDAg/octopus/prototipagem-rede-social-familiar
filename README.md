# Prototipagem Rede Social Familiar

# Documentação

## Sobre o Projeto

Projeto destinado à fazer uma prova de conceitos sobre uma ideia de rede social móvel para UDF (Universidade da Família).

Para saber mais [clique aqui.](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/home/)

Para acessar o cronograma [clique aqui.](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/cronograma)

Para seguir o tutorial de instalação [clique aqui.](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Documentação-da-Solução#montagem-do-ambiente-do-desenvolvimento)

## Andamento do Projeto

- [X] [Definições de Projeto](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/home/)
- [X] [Documentação da Solução](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Documentação-da-Solução)
- [X] Desenvolvimento da Solução (Sprint 1)
- [X] Desenvolvimento da Solução (Sprint 2)
- [X] Desenvolvimento da Solução (Sprint 3)
- [ ] Fechamento do Projeto

## Equipe

- [Iam Caio (CTO)](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Iam-Caio)

- [José Pedro (CTO)](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/José-Pedro)

- [Luiz Fernando Bergamin](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Luiz-Bergamin)

- [Dério Lucas](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Derio-Lucas)

- [Pedro Henrique](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Pedro)

- [Elaine Cristina](https://gitlab.com/BDAg/octopus/prototipagem-rede-social-familiar/wikis/Elaine-Cristina)
