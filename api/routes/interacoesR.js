const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const interacoesController = require('../controllers/interacoesC');

router.post('/make-interaction', authorize(Roles.User), interacoesController.makeInteraction);

router.post('/remove-interaction', authorize(Roles.User), interacoesController.removeInteraction);


module.exports = router;
