const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const emocaoController = require('../controllers/emocaoC');

router.post('/emocao', authorize(Roles.User), emocaoController.emocao);
module.exports = router;