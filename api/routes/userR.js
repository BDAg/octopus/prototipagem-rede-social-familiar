const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const userController = require('../controllers/userC');

router.get('/list', authorize(Roles.Admin), userController.listAll);

router.post('/remove', authorize(Roles.Admin), userController.removeById);

router.post('/create-user', userController.createUser);

router.post('/login', userController.login);

router.post('/users-info', authorize(Roles.User), userController.searchUsers);

router.post('/verify-token', userController.verifyToken);

router.post('/forgot-password', userController.esqueciSenha);

router.post('/verify-code', userController.verifyCode);

router.post('/change-password-code', userController.changePasswordCode);

router.post('/change-password', authorize(Roles.User), userController.redefinePassword);

router.post('/verify-username', userController.verifyUsername);

router.post('/verify-email', userController.verifyEmail);

router.post('/update-foto', authorize(Roles.User), userController.updateFotoPerfil);

module.exports = router;