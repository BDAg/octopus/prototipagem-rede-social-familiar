const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const familyController = require('../controllers/familiaC');

router.post('/create-family', authorize(Roles.User), familyController.createFamily);

router.post('/add-member', authorize(Roles.User), familyController.addMember);

router.post('/join-family', authorize(Roles.User), familyController.joinFamily);

router.post('/find-family', authorize(Roles.User), familyController.findFamily);

router.post('/update-foto', authorize(Roles.User), familyController.updateFotoPerfil);

module.exports = router;
