const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const midiaController = require('../controllers/midiaC');

router.post('/list', authorize(Roles.User), midiaController.listAll);

router.post('/list-user', authorize(Roles.User), midiaController.listUser);

router.post('/list-family', authorize(Roles.User), midiaController.listFamily);

router.post('/post-midia', authorize(Roles.User), midiaController.postMidia);

router.post('/delete-midia', authorize(Roles.User), midiaController.deleteMidia);

router.post('/upload', authorize(Roles.User), midiaController.upload);

router.post('/like', authorize(Roles.User), midiaController.like);

router.post('/verify-feed', authorize(Roles.User), midiaController.verifyFeed);

module.exports = router;
