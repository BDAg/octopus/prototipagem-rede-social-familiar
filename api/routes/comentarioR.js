const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const comentarioController = require('../controllers/comentarioC');

router.post('/list-commentaries', authorize(Roles.User), comentarioController.listAll);

router.post('/make-commentary', authorize(Roles.User), comentarioController.postComment);

router.post('/delete-commentary', authorize(Roles.User), comentarioController.deleteComment);

router.post('/edit-commentary', authorize(Roles.User), comentarioController.editComment);

module.exports = router;
