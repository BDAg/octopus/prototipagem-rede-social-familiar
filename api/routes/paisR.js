const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const countryController = require('../controllers/paisC');

router.get('/list', authorize(Roles.User), countryController.listAllCountries);

module.exports = router;
