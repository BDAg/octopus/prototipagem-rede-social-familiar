const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Roles = require('../_helpers/role');

const paisController = require('../controllers/paisC');
const cidadeController = require('../controllers/cidadeC');

router.get('/countries', paisController.listAllCountries);
router.post('/country', paisController.SearchCountry);

router.post('/cities', cidadeController.SearchCities);

module.exports = router;