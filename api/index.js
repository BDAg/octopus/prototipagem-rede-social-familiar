const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require('body-parser');
const path = require('path');
var fs = require("fs");
const serveStatic = require('serve-static');

const errorHandler = require('./_helpers/error-handler');

const userRoutes = require('./routes/userR');
const utilsRoutes = require('./routes/utilsR');
const familyRoutes = require('./routes/familiaR');
const midiaRoutes = require('./routes/midiaR');
const emocaoRoutes = require('./routes/emocaoR');
const commentRoutes = require('./routes/comentarioR');

const app = express();

const dir = path.join(__dirname, 'images');

app.use(cors());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(express.json({limit: '50mb'}));
app.use(bodyParser.json());

let diretorios = [
                  __dirname + '/images/',
                  __dirname + '/images/user/',
                  __dirname + '/images/family/'
                ];

diretorios.forEach(
  (value) => {
    if (!fs.existsSync(value)) {
      fs.mkdirSync(value);
    }
  }
);

mongoose.connect(
  "mongodb://api:octopus2019@befamily-shard-00-00-zbm2o.mongodb.net:27017,befamily-shard-00-01-zbm2o.mongodb.net:27017,befamily-shard-00-02-zbm2o.mongodb.net:27017/befamily?ssl=true&replicaSet=befamily-shard-0&authSource=admin&retryWrites=true",
  { useNewUrlParser: true }
);

// app.use('/images', express.static(dir));
app.use('/images', serveStatic(dir));
app.use('/user', userRoutes);
app.use('/util', utilsRoutes);
app.use('/family', familyRoutes);
app.use('/midia', midiaRoutes);
app.use('/emocao', emocaoRoutes);
app.use('/comment', commentRoutes);

app.use(errorHandler);

let port = process.env.PORT || 4000;
let host = '0.0.0.0';

app.listen(port, host, () => {
  console.log("Recebendo requests na porta "+port);
});