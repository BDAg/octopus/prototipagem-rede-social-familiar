const Pais = require('../models/paisM');

exports.listAllCountries = function(req, res, next) {

    Pais.find(function(err, countries) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            return res.status(200).json({
                'error': null,
                'response': countries
            });
        }
    });

}

exports.SearchCountry = function(req, res, next) {

    let pais = req.body.pais;

    if (pais == undefined) {
        return res.status(406).json({
            'error': 'Invalid params!',
            'response': null
        });
    } else {
        Pais.findOne({ nome: pais }, function(err, country) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
    
                return res.status(200).json({
                    'error': null,
                    'response': country
                });
    
            }
        });
    }

}

