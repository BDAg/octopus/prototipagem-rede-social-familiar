const jwt = require('jsonwebtoken');
const ObjectId = require('mongoose').Types.ObjectId;

const User = require('../models/userM');
const secret = require('../_helpers/config');
const Emocao = require('../models/emocaoM');

exports.emocao = function(req, res, next){
    let emocao = req.body.emocao;
    let token = req.body.token;
    if(emocao==undefined || token==undefined){
        return res.status(406).json({
            'response': null, 
            'error': 'Invalid params'
        });
        
    } else{

        let tokenDecoded=jwt.verify(token,secret.secret);

        let emocaoUser= new Emocao({emocao:emocao, idUsuario: ObjectId(tokenDecoded['idUser']) });

        emocaoUser.save(function(err, result){
            if(err){
                return res.status(406).json({
                    'response':null, 
                    'error':err
                });
            } else{
                return res.status(201).json({
                   'error':null,
                   'response': {
                       'message': 'Success',
                       'data': result
                   }
                })
            }

        });
    }
}