const User = require('../models/userM');
const Family = require('../models/familiaM');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const secret = require('../_helpers/config');
const Role = require('../_helpers/role');

const ObjectId = require('mongoose').Types.ObjectId;

let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'octopusprojectoficial@gmail.com', // generated ethereal user
        pass: 'lula2018' // generated ethereal password
    },
    tls: {
        rejectUnauthorized: false 
    }
});

exports.listAll = async function(req, res, next) {
    await User.find({}, function(err, users) {
        if (err) {
            return res.status(406).json({
                'response' : null,
                'error' : err
            });
        } else {
            return res.status(200).json({
                'response' : users,
                'error' : null
            });
        }
    });
};

exports.createUser = function(req, res, next) {

    let dados = {
        nome: null,
        sobrenome: null,
        dataNascimento: null,
        username: null,
        profissao: null,
        email: null,
        senha: null,
        telefone: null,
        genero: null,
        descricao: null,
        cidade: null,
        religiao: null,
        role: Role.User
    };
    
    for (let key in req.body) {
        if (dados.hasOwnProperty(key)) {
            dados[key] = req.body[key];
        }
    }

    let condition = dados.nome == null || dados.dataNascimento == null || dados.username == null || 
                    dados.email == null || dados.senha == null || dados.telefone == null
                    || dados.cidade == null;

    if (condition) {
        return res.status(406).json({
            'response': null,
            'error' : 'Invalid params!'
        });
    } else {

        for (let key in dados) {
            if (dados[key] == null) {
                delete dados[key];
            }
        }

        dados.cidade = ObjectId(dados.cidade);

        let dataModify = new Date(+dados.dataNascimento);
        dados.dataNascimento = dataModify;

        bcrypt.hash(dados.senha, 10, function(err, hash) {
            if (err) {
                return res.status(406).json({
                    'response' : null,
                    'error' : err
                });
            } else {

                dados.senha = hash;
                let newUser = new User(dados);

                newUser.save(function(err, user) {
                    if (err) {
                        return res.status(406).json({
                            'response': null,
                            'error' : err
                        });
                    } else {

                        let infoToken = {
                            email: user.email,
                            idUser: user._id,
                            nome: user.nome,
                            role: user.role
                        }

                        let token = jwt.sign(
                            infoToken,
                            secret.secret,
                            { expiresIn: "2h" }
                        );

                        return res.status(201).json({
                            'error': null,
                            'response' : {
                                'message': 'Created!',
                                'data': user,
                                'token': token
                            }
                        });
                    }
                });

            }

        });

    }

};

exports.removeById = function(req, res, next) {

    let userId = req.body._id;

    if (userId == undefined) {

        return res.status(406).json({
            'response' : null,
            'error' : '_id required'
        });

    } else {

        User.remove({ _id : userId }, function(err) {
            if (err) {
                return res.status(406).json({
                    'response' : null,
                    'error' : err
                });
            } else {
                return res.status(400).json({
                    'response' : 'Success!',
                    'error' : null
                });
            }
        });

    }
};

exports.login = function(req, res, next) {

    let emailOrUsername = req.body.emailOrUsername;
    let senha = req.body.senha;

    if (emailOrUsername == undefined || senha == undefined) {
        return res.status(406).json({
            'response' : null,
            'error' : 'Invalid params!'
        });
    } else {

        User.findOne({ $or: [ { email: emailOrUsername }, { username: emailOrUsername } ] }, function(err, user) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (!user) {
                    return res.status(406).json({
                        'response': null,
                        'error': 'User not found' 
                    });
                } else {

                    bcrypt.compare(senha, user.senha, function(err, autenticado) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {
                            if (!autenticado) {
                                return res.status(401).json({
                                    'error': null,
                                    'response': {
                                        'auth': false,
                                        'message': 'Not authenticated!'
                                    }
                                });
                            } else {

                                Family.find( { membros : { $in : user._id } }, function(err, families) {
                                    if (err) {
                                        return res.status(406).json({
                                            'response': null,
                                            'error': err
                                        });
                                    } else {
                                        let infoToken = {
                                            'email': user.email,
                                            'idUser': user._id,
                                            'nome': user.nome,
                                            'role': user.role
                                        };
            
                                        let infoUser = {
                                            'idUser': user._id,
                                            'nome': user.nome,
                                            'sobrenome': user.sobrenome,
                                            'email': user.email,
                                            'fotoPerfil': user.fotoPerfil,
                                            'dataNascimento': user.dataNascimento,
                                            'username': user.username,
                                            'profissao': user.profissao,
                                            'telefone': user.telefone,
                                            'genero': user.genero,
                                            'cidade': user.cidade,
                                            'criadoEm': user.criadoEm,
                                            'religiao': user.religiao,
                                            'descricao': user.descricao,
                                            'families': families
                                        };
            
                                        let token = jwt.sign(
                                                                infoToken,
                                                                secret.secret,
                                                                { expiresIn: "365d" }
                                                            );
        
                                        return res.status(201).json({
                                            'error' : null,
                                            'response' : {
                                                'auth': true,
                                                'message': 'Authenticated!',
                                                'token': token,
                                                'data': infoUser
                                            }
                                        });
                                    }
                                });
    
                            }
                        }
                    });

                }

            }
        });

    }

};

exports.searchUsers = function(req, res, next) {

    console.log('Chamou');

    let token = req.body.token;
    let users = req.body.users;

    console.log(users);

    if(token == undefined || users == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid Params'
        });
    } else {

        users = JSON.parse(users);

        let usersId = [];

        users.forEach(
            (value) => {
                usersId.push( ObjectId(value) );
            }
        );

        if (!usersId.length) {
            return res.status(406).json({
                'response': null,
                'error': 'Length array invalid!'
            });
        } else {

            User.find( { "_id": { "$in": usersId } }).sort({ 'nome': 1 }).exec(function(err, usersinfo) {
                if (err) {
                    return res.status(406).json({
                        'response': null,
                        'error': err
                    });
                } else {

                    if (!usersinfo) {
                        return res.status(404).json({
                            'response': null,
                            'error': 'Not found users'
                        });
                    } else {
                        return res.status(200).json({
                            'error': null,
                            'response': {
                                'message': 'Success!',
                                'data': usersinfo
                            }
                        });
                    }

                }
            });
        }
    }
}

exports.verifyToken = function(req, res, next) {

    let token = req.body.token;

    if (token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        tokenDecoded = jwt.verify(token, secret.secret);

        let idUser = tokenDecoded['idUser'];

        User.findById( idUser, (err, user) => {
            if(err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (!user) {
                    return res.status(406).json({
                        'response': null,
                        'error': 'Invalid token!'
                    });
                } else {

                    let verify = isTokenExpired(token);

                    if (!verify) {
                        return res.status(406).json({
                            'response': null,
                            'error': 'Invalid token!'
                        });
                    } else {
                        return res.status(200).json({
                            'error': null,
                            'response': 'Valid token!'
                        });
                    }
                }
            }
        });
    }
}

isTokenExpired = (token) => {
    try {
      const date = new Date(0);
      const decoded = jwt.verify(token, secret.secret);
      date.setUTCSeconds(decoded.exp);
      return date.valueOf() > new Date().valueOf();
    } catch (err) {
      return false;
    }
};

exports.esqueciSenha = function(req, res, next) {

    let email = req.body.email;

    if (email == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        User.findOne({ email: email }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (!result) {
                    return res.status(404).json({
                        'response': null,
                        'error': 'Not found account!'
                    });
                } else {

                    let code = Math.floor(100000 + Math.random() * 900000);

                    bcrypt.hash(code.toString(), 10, function(err, codeHash) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {
                            User.updateOne( { _id: result._id }, { $set : { codeSecurity: codeHash } }, function(err, resultUpdate) {
                                if (err) {
                                    return res.status(406).json({
                                        'response': null,
                                        'error': err
                                    });
                                } else {
    
                                    conteudo_email = `Oi ${result.nome + ' ' + result.sobrenome},<br><br>
                                    Esqueceu sua senha? Tudo bem, não nos esquecemos de você! Copie o código para redefinir sua senha.<br><br>
                                    Código: ${code} <br><br>
                                    `;
                                
                                    var mailOptions = {
                                        from: 'octopusprojectoficial@gmail.com',
                                        to: email,
                                        subject: 'Recuperação de Conta',
                                        html: conteudo_email
                                    };
                        
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            return res.status(500).res({
                                                'response': null,
                                                'error': error
                                            });
                                        } else {
                                            return res.status(200).json({
                                                'error': null,
                                                'response': {
                                                    'message': 'Success!',
                                                    'info': info
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });                                  
                }
            }
        });
    }
};

exports.verifyCode = function(req, res, next) {

    let code = req.body.code;
    let email = req.body.email;

    if (code == undefined || email == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        User.findOne({ email: email }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (!result) {
                    return res.status(404).json({
                        'response': null,
                        'error': 'Not found account!'
                    });
                } else {

                    bcrypt.compare(code.toString(), result.codeSecurity, function(err, auth) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {

                            if (!auth) {
                                return res.status(406).json({
                                    'error': null,
                                    'response': {
                                        'auth': false
                                    }
                                });
                            } else {
                                return res.status(202).json({
                                    'error': null,
                                    'response': {
                                        'auth': true
                                    }
                                });
                            }

                        }
                    });                                  
                }
            }
        });
    }
}

exports.changePasswordCode = function(req, res, next) {

    let code = req.body.code;
    let email = req.body.email;

    let newPassword = req.body.newPassword;

    if (code == undefined || email == undefined || newPassword == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        User.findOne({ email: email }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (!result) {
                    return res.status(404).json({
                        'response': null,
                        'error': 'Not found account!'
                    });
                } else {

                    bcrypt.compare(code.toString(), result.codeSecurity, function(err, auth) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {

                            if (!auth) {
                                return res.status(406).json({
                                    'error': 'Not authorized',
                                    'response': null
                                });
                            } else {
                                bcrypt.hash(newPassword, 10, function(err, senhaHash) {
                                    if (err) {
                                        return res.status(406).json({
                                            'response': null,
                                            'error': err
                                        });
                                    } else {
                                        User.updateOne( { _id: result._id }, { $set : { senha: senhaHash } }, function(err, resultUpdate) {
                                            if (err) {
                                                return res.status(406).json({
                                                    'response': null,
                                                    'error': err
                                                });
                                            } else {

                                                let code = Math.floor(100000 + Math.random() * 900000);

                                                bcrypt.hash(code.toString(), 10, function(err, codeHash) {
                                                    if (err) {
                                                        return res.status(406).json({
                                                            'response': null,
                                                            'error': err
                                                        });
                                                    } else {
                                                        User.updateOne( { _id: result._id }, { $set : { codeSecurity: codeHash } }, function(err, resultUpdate) {
                                                            if (err) {
                                                                return res.status(406).json({
                                                                    'response': null,
                                                                    'error': err
                                                                });
                                                            } else {
                                                                return res.status(200).json({
                                                                    'error': null,
                                                                    'response': {
                                                                        'message': 'Success!',
                                                                        'result': resultUpdate
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });                                  
                }
            }
        });
    }
}

exports.redefinePassword = function(req, res, next) {

    let token = req.body.token;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;

    if (token == undefined || oldPassword == undefined || newPassword == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {

        tokenDecoded = jwt.verify(token, secret.secret);

        let idUser = tokenDecoded['idUser'];

        User.findById(ObjectId(idUser), function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (!result) {
                    return res.status(406).json({
                        'response': null,
                        'error': 'User not Found!'
                    });
                } else {
                    bcrypt.compare(oldPassword, result.senha, function(err, equal) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {
                            if (!equal) {
                                return res.status(406).json({
                                    'response': null,
                                    'error': 'Invalid password!'
                                });
                            } else {
                                bcrypt.hash(newPassword, 10, function(err, passwordHash) {
                                    if (err) {
                                        return res.status(406).json({
                                            'response': null,
                                            'error': err
                                        });
                                    } else {
                                        User.updateOne(
                                            { _id: ObjectId(idUser) },
                                            {
                                                $set: { senha: passwordHash }
                                            },
                                            function(err, result) {
                                                if (err) {
                                                    return res.status(406).json({
                                                        'response': null,
                                                        'error': err
                                                    });
                                                } else {
                                                    return res.status(200).json({
                                                        'error': null,
                                                        'response': {
                                                            'message': 'Success!',
                                                            'data': result
                                                        }
                                                    });
                                                }
                                            }
                                        )
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
}

exports.verifyUsername = function(req, res, next) {

    let username = req.body.username;

    if (username == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        User.findOne({ username: username }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (!result) {
                    return res.status(200).json({
                        'error': null,
                        'response': {
                            'message': 'Valid username!'
                        }
                    });
                } else {
                    return res.status(406).json({
                        'error': null,
                        'response': {
                            'message': 'Invalid username!'
                        }
                    });
                }

            }
        });

    }

}

exports.verifyEmail = function(req, res, next) {

    let email = req.body.email;

    if (email == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        User.findOne({ email: email }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (!result) {
                    return res.status(200).json({
                        'error': null,
                        'response': {
                            'message': 'Valid email!'
                        }
                    });
                } else {
                    return res.status(406).json({
                        'error': null,
                        'response': {
                            'message': 'Invalid email!'
                        }
                    });
                }

            }
        });

    }

}

exports.updateFotoPerfil = function(req, res, next) {

    let token = req.body.token;
    let path = req.body.path;

    if (token == undefined || path == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        User.updateOne({ _id: tokenDecoded['idUser'] }, { $set : { fotoPerfil: path } }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                return res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'Success!',
                        'data': result
                    }
                });
            }
        });

    }
    
}