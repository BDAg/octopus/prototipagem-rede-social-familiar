const jwt = require('jsonwebtoken');
const ObjectId = require('mongoose').Types.ObjectId;

const Comentario = require('../models/comentarioM');
const Midia = require('../models/midiaM');

const User = require('../models/userM');
const secret = require('../_helpers/config');

exports.listAll = function(req, res, next) {

    let idMidia = req.body.idMidia;

    if (idMidia == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {
        Comentario.find({ idMidia: idMidia }).sort({ dateTime: -1 }).lean().exec(function(err, comentarios) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': 'Invalid params!'
                });
            } else {
                let comment = [];
                let usuarios = [];
                for (let comentario of comentarios) {
                    comment.push(comentario);
                    if (!(comentario['idUsuario'] in usuarios)) {
                        usuarios.push(comentario['idUsuario']);
                    }
                }

                User.find({ _id: { $in: usuarios } }, function(err, users) {
                    if (err) {
                        return res.status(406).json({
                            'response' : null,
                            'error' : err
                        });
                    } else {
                        for (let index in comment) {
                            let idUser = comment[index]['idUsuario'];
                            for (user of users) {
                                if (user._id.toString() == idUser.toString()) {

                                    comment[index]['nome'] = user.nome;
                                    comment[index]['sobrenome'] = user.sobrenome;
                                    comment[index]['foto'] = user.fotoPerfil;

                                    break;
                                }
                            }
                        }

                        return res.status(200).json({
                            'error' : null,
                            'response' : {
                                'message': 'Success',
                                'data': comment
                            }
                        });

                    }
                });
            }
        });
    }
}

exports.postComment = function(req, res, next) {

    let token = req.body.token;

    let dados = {
        'idMidia': null,
        'comentario': null
    }

    for (let i in req.body) {
        if (dados.hasOwnProperty(i)) {
            dados[i] = req.body[i];
        }
    }

    let condition = dados.idMidia == undefined || token == undefined || dados.comentario == undefined

    if (condition) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);
        dados.idUsuario = tokenDecoded['idUser'];

        let comment = new Comentario(dados);

        comment.save(function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                Comentario.find({ 'idMidia': dados.idMidia }).count((err, count) => {
                    if (err) {
                        return res.status(406).json({
                            'response': null,
                            'error': err
                        });
                    } else {

                        Midia.updateOne({ '_id': dados.idMidia }, { $set: { 'comment': count } }, (err, upMidia) => {
                            if (err) {
                                return res.status(406).json({
                                    'response': null,
                                    'error': err
                                });
                            } else {
                                return res.status(201).json({
                                    'error': null,
                                    'response': {
                                        'message': 'success',
                                        'data': result
                                    }
                                });
                            }
                        });

                    }
                });
            }
        });

    }
}

exports.deleteComment = function(req, res, next) {

    let idComentario = req.body.idComentario;
    let token = req.body.token;

    if (idComentario == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);
        let idUsuario = tokenDecoded['idUser'];

        Comentario.find({ _id: idComentario, idUsuario: idUsuario }, function(err, comentarios) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (comentarios.length) {
                    Comentario.deleteOne(
                        { _id: idComentario },
                        function(err) {
                            if (err) {
                                return res.status(406).json({
                                    'response': null,
                                    'error': err
                                });
                            } else {
                                Comentario.find({ 'idMidia': comentarios[0].idMidia }).count((err, count) => {
                                    if (err) {
                                        return res.status(406).json({
                                            'response': null,
                                            'error': err
                                        });
                                    } else {
                                        Midia.updateOne({ '_id': comentarios[0].idMidia }, { $set: { 'comment': count } }, (err, upMidia) => {
                                            if (err) {
                                                return res.status(406).json({
                                                    'response': null,
                                                    'error': err
                                                });
                                            } else {
                                                return res.status(200).json({
                                                    'error': null,
                                                    'response': {
                                                        'message': 'Deleted'
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    );
                } else {
                    return res.status(401).json({
                        'response': null,
                        'error': 'Rejected Action'
                    });
                }
            }
        });
    }
}

exports.editComment = function(req, res, next) {
    let idComentario = req.body.idComentario; 
    let comentario = req.body.comentario;

    if (idComentario == undefined || comentario == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {
        Comentario.updateOne( { _id: idComentario }, { $set : { comentario: comentario } }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (result.n){
                    return res.status(200).json({
                        'error' : null,
                        'response' : 'success'
                    });
                } else {
                    return res.status(404).json({
                        'response' : null,
                        'error' : 'comment not found'
                    });
                }
            }
        });
    }
}


        