const jwt = require('jsonwebtoken');
const ObjectId = require('mongoose').Types.ObjectId;
const fs = require("fs");
const sizeOf = require('buffer-image-size');

const Midia = require('../models/midiaM');
const Familia = require('../models/familiaM');
const User = require('../models/userM');
const secret = require('../_helpers/config');

exports.listAllPage = function(req, res, next) {

    let idFamilia = req.body.idFamilia;
    let token = req.body.token;
    let page = req.body.page;
    let pageLimit = 5;

    if (idFamilia == undefined || token == undefined || page == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);
        
        try {
            idFamilia = JSON.parse(idFamilia);
        } catch(e) {
            idFamilia = [ idFamilia ]
        }

        Familia.find({ _id: { $in : idFamilia }, membros: { $in : [ ObjectId( tokenDecoded['idUser'] ) ] } }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (result.length) {
                    let dados = Array.from(result);

                    let families = []

                    for (let dado of dados) {
                        families.push(dado['_id']);
                    }

                    Midia.find({ idFamilia: { $in: families } }).sort({ dateTime: -1 }).skip( (page - 1) * pageLimit  ).limit(pageLimit).lean().exec(function(err, midias) {
                        if (err) {
                            return res.status(406).json({
                                'response' : null,
                                'error' : err
                            });
                        } else {
                            let feed = [];

                            if (!midias) {
                                return res.status(200).json({
                                    'error' : null,
                                    'response' : {
                                        'message': 'Fail',
                                        'data': midias
                                    }
                                });
                            } else {

                                let usuarios = [];
                                for (let midia of midias) {
                                    feed.push(midia);
                                    if (!(midia['idUsuario'] in usuarios)) {
                                        usuarios.push(midia['idUsuario']);
                                        for (let likeUser of midia['like']) {
                                            if (!(likeUser in usuarios)) {
                                                usuarios.push(likeUser);
                                            }
                                        }
                                    }
                                }

                                User.find({ _id: { $in: usuarios } }, function(err, users) {
                                    if (err) {
                                        return res.status(406).json({
                                            'response' : null,
                                            'error' : err
                                        });
                                    } else {

                                        for (let index in feed) {
                                            idUsuario = feed[index]['idUsuario']
                                            for (user of users) {
                                                if (user._id.toString() == idUsuario.toString()) {
                                                    feed[index]['nome'] = user.nome;
                                                    feed[index]['sobrenome'] = user.sobrenome;
                                                    feed[index]['foto'] = user.fotoPerfil;
                                                    feed[index]['username'] = user.username;
                                                }
                                            }
                                        }
                                        
                                        for (let index in feed) {
                                            for (let user of users) {
                                                for (let indexLike in feed[index]['like']) {
                                                    if (user._id.toString() == feed[index]['like'][indexLike].toString()) {
                                                        feed[index]['like'][indexLike] = {
                                                            '_id': user._id.toString(),
                                                            'nome': user.nome,
                                                            'sobrenome': user.sobrenome,
                                                            'foto': user.fotoPerfil,
                                                            'username': user.username
                                                        };
                                                        break
                                                    }
                                                }
                                            }
                                        }

                                        return res.status(200).json({
                                            'error' : null,
                                            'response' : {
                                                'message': 'Success',
                                                'data': feed
                                            }
                                        });

                                    }
                                });
                            }
                        }
                    });
                } else {
                    return res.status(401).json({
                        'response': null,
                        'error': 'List reject'
                    });
                }
            }
        });
    }
};

exports.listAll = function(req, res, next) {

    let idFamilia = req.body.idFamilia;
    let token = req.body.token;

    if (idFamilia == undefined || token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);
        
        try {
            idFamilia = JSON.parse(idFamilia);
        } catch(e) {
            idFamilia = [ idFamilia ]
        }

        Familia.find({ _id: { $in : idFamilia }, membros: { $in : [ ObjectId( tokenDecoded['idUser'] ) ] } }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (result.length) {
                    let dados = Array.from(result);

                    let families = []

                    for (let dado of dados) {
                        families.push(dado['_id']);
                    }

                    Midia.find({ idFamilia: { $in: families } }).sort({ dateTime: -1 }).lean().exec(function(err, midias) {
                        if (err) {
                            return res.status(406).json({
                                'response' : null,
                                'error' : err
                            });
                        } else {
                            let feed = [];

                            if (!midias) {
                                return res.status(200).json({
                                    'error' : null,
                                    'response' : {
                                        'message': 'Fail',
                                        'data': midias
                                    }
                                });
                            } else {

                                let usuarios = [];
                                for (let midia of midias) {
                                    feed.push(midia);
                                    if (!(midia['idUsuario'] in usuarios)) {
                                        usuarios.push(midia['idUsuario']);
                                        for (let likeUser of midia['like']) {
                                            if (!(likeUser in usuarios)) {
                                                usuarios.push(likeUser);
                                            }
                                        }
                                    }
                                }

                                User.find({ _id: { $in: usuarios } }, function(err, users) {
                                    if (err) {
                                        return res.status(406).json({
                                            'response' : null,
                                            'error' : err
                                        });
                                    } else {

                                        for (let index in feed) {
                                            idUsuario = feed[index]['idUsuario']
                                            for (user of users) {
                                                if (user._id.toString() == idUsuario.toString()) {
                                                    feed[index]['nome'] = user.nome;
                                                    feed[index]['sobrenome'] = user.sobrenome;
                                                    feed[index]['foto'] = user.fotoPerfil;
                                                    feed[index]['username'] = user.username;
                                                }
                                            }
                                        }
                                        
                                        for (let index in feed) {
                                            for (let user of users) {
                                                for (let indexLike in feed[index]['like']) {
                                                    if (user._id.toString() == feed[index]['like'][indexLike].toString()) {
                                                        feed[index]['like'][indexLike] = {
                                                            '_id': user._id.toString(),
                                                            'nome': user.nome,
                                                            'sobrenome': user.sobrenome,
                                                            'foto': user.fotoPerfil,
                                                            'username': user.username
                                                        };
                                                        break
                                                    }
                                                }
                                            }
                                        }

                                        return res.status(200).json({
                                            'error' : null,
                                            'response' : {
                                                'message': 'Success',
                                                'data': feed
                                            }
                                        });

                                    }
                                });
                            }
                        }
                    });
                } else {
                    return res.status(401).json({
                        'response': null,
                        'error': 'List reject'
                    });
                }
            }
        });
    }
};

exports.verifyFeed = function(req, res, next) {

    let timestamp = req.body.timestamp;
    let idFamilia = req.body.idFamilia;
    let token = req.body.token;

    if (idFamilia == undefined || token == undefined || timestamp == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);
        
        try {
            idFamilia = JSON.parse(idFamilia);
        } catch(e) {
            idFamilia = [ idFamilia ]
        }

        Familia.find({ _id: { $in : idFamilia }, membros: { $in : [ ObjectId( tokenDecoded['idUser'] ) ] } }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                if (result.length) {

                    let dados = Array.from(result);

                    let families = []

                    for (let dado of dados) {
                        families.push(dado['_id']);
                    }

                    Midia.find({ idFamilia: { $in: families } }).sort({ dateTime: -1 }).limit(1).lean().exec(function(err, midias) {
                        if (err) {
                            return res.status(406).json({
                                'response' : null,
                                'error' : err
                            });
                        } else {
                            if (!midias.length) {
                                return res.status(200).json({
                                    'error': null,
                                    'response' : {
                                        'message': 'success',
                                        'update': false
                                    }
                                });
                            } else {
                                if (Number(midias[0].dateTime) > timestamp) {
                                    return res.status(200).json({
                                        'error': null,
                                        'response' : {
                                            'message': 'success',
                                            'update': true
                                        }
                                    });
                                } else {
                                    return res.status(200).json({
                                        'error': null,
                                        'response' : {
                                            'message': 'success',
                                            'update': false
                                        }
                                    });
                                }
                            }
                        }
                    });
                } else {
                    return res.status(404).json({
                        'error': null,
                        'message': 'Not found'
                    });
                }
            }
        });
    }
};

exports.listUser = function(req, res, next) {

    let token = req.body.token;

    if (token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid Params'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        let idUsuario = tokenDecoded['idUser'];

        Midia.find({ idUsuario : idUsuario }).sort({ dateTime: -1 }).exec(function(err, midias) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                return res.status(200).json({
                    'error': null,
                    'response': midias
                })
            }
        });
    }
}

exports.listFamily = function(req, res, next) {

    let idFamily = req.body.idFamily;

    if (idFamily == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        Midia.find({ idFamilia: { $in: [ ObjectId(idFamily) ] } }).sort({ dateTime: -1 }).exec(function(err, midias) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                return res.status(200).json({
                    'error': null,
                    'response': midias
                });

            }
        });

    }

}

exports.postMidia = function(req, res, next) {

    let dados = {
        'tipo': null,
        'legenda': null,
        'checkin': null,
        'idFamilia': null,
        'path': null,
        'height': null,
        'width': null
    }

    let token = req.body.token;

    for (let i in req.body) {
        if (dados.hasOwnProperty(i)) {
            dados[i] = req.body[i];
        }
    }

    let condition = dados.tipo == undefined || dados.idFamilia == undefined 
    || dados.path == undefined || token == undefined || dados.height == undefined || dados.width == undefined;

    if (condition) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        for (let key in dados) {
            if (dados[key] == null) {
                delete dados[key];
            }
        }

        let tokenDecoded = jwt.verify(token, secret.secret);
        dados['idUsuario'] = ObjectId(tokenDecoded['idUser']);
        dados.height = +dados.height;
        dados.width = +dados.width;

        try {
            dados.idFamilia = JSON.parse(dados.idFamilia);
        } catch(e) {
            return res.status(406).json({
                'response': null,
                'error': 'Invalid idFamilia'
            });
        }

        let aux = [];

        for (let dado of dados.idFamilia) {
            aux.push(ObjectId(dado));
        }

        dados.idFamilia = aux;

        Familia.find({ _id: { $in : dados.idFamilia }, membros: { $in : [ ObjectId( tokenDecoded['idUser'] ) ] } }, function(err, result) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                if (result.length) {

                    let midia = new Midia(dados);

                    midia.save(function(err, result) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {
                            return res.status(201).json({
                                'error': null,
                                'response': {
                                    'message': 'success',
                                    'data': result
                                }
                            });
                        }
                    });

                } else {
                    return res.status(401).json({
                        'response': null,
                        'error': 'Post reject'
                    });
                }
            }
        });
    }
}

exports.deleteMidia = function(req, res, next) {
    let token = req.body.token;
    let idMidia = req.body.idMidia;

    if (idMidia == undefined || token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        Midia.deleteOne(
            { 
                _id: idMidia,
                idUsuario: ObjectId(tokenDecoded['idUser'])
            },
            function(err, data) {
                if (err) {
                    return res.status(406).json({
                        'response': null,
                        'error': err
                    });
                } else {

                    return res.status(200).json({
                        'error': null,
                        'response': {
                            'message': 'Deleted'
                        }
                    })
                }
            }
        );
    }
}

exports.upload = function(req, res, next) {

    let token = req.body.token;
    let nomeImg = req.body.nomeImg;
    let image = req.body.image;

    if (token == undefined || nomeImg == undefined || image == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params'
        });
    } else {
        
        let tokenDecoded = jwt.verify(token, secret.secret);

        let realImage = Buffer.from(image,"base64");
        let dimensions = sizeOf(realImage);

        let idToken;

        if ('idUser' in tokenDecoded) {
            idToken = 'user/' + tokenDecoded['idUser'];
        } else if ('idFamily' in tokenDecoded) {
            idToken = 'family/' + tokenDecoded['idFamily']
        } else {
            return res.status('406').json({
                'response': null,
                'error': 'Invalid Token'
            });
        }

        let diretorio = __dirname + '/../images/'+ idToken;

        if (!fs.existsSync(diretorio)) {
            fs.mkdirSync(diretorio);
        }

        nomeImg = nomeImg + new Date().getTime().toString()
        
        fs.writeFile(diretorio +'/' + nomeImg + '.jpg', realImage, function(err) {
            if(err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                return res.status(201).json({
                    'error': null,
                    'response': {
                        'message': 'Success',
                        'path': idToken + '/' + nomeImg + '.jpg',
                        'height': dimensions.height,
                        'width': dimensions.width
                    }
                });
            }
         });
    }

}

exports.like = function(req, res, next) {
    let idMidia=req.body.idMidia;
    let token = req.body.token;
    
    if(idMidia == undefined || token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'invalid params'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);
        let idUsuario = tokenDecoded['idUser'];

        Midia.find({ _id: idMidia, like: { $in: [ ObjectId(idUsuario) ] } }, function(err, midias) {
            if(err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else{
                if(midias.length){
                    Midia.updateOne({ _id : idMidia}, { $pull: { like: ObjectId(idUsuario) } }, function(err, result) {
                        if(err){
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else{
                            return res.status(200).json({
                                'error': null,
                                'response': 'Success'
                            });
                        }
                    });
                } else {
                    Midia.updateOne({ _id : idMidia}, { $addToSet: { like: ObjectId(idUsuario) } }, function(err, result) {
                        if(err){
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else{
                            return res.status(200).json({
                                'error': null,
                                'response': 'Success'
                            });
                        }
                    });
                }
            }
        })
    }

}