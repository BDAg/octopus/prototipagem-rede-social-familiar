const Cidade = require('../models/cidadeM');

exports.listAllCitys = function(req, res, next) {

    Cidade.find(function(err, citys) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            return res.status(200).json({
                'error': null,
                'response': citys
            });
        }
    });

}

exports.SearchCities = function(req, res, next) {

    let cidade = req.body.cidade;

    if (cidade == undefined) {
        return res.status(406).json({
            'error': 'Invalid params!',
            'response': null
        });
    } else {
        Cidade.find({ nome: { $regex: cidade, $options: 'i' } }, function(err, city) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
    
                return res.status(200).json({
                    'error': null,
                    'response': city
                });
    
            }
        });
    }

}

