const Estado = require('../models/estadoM');

exports.listAllEstados = function(req, res, next) {

    Estado.find(function(err, estados) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            return res.status(200).json({
                'error': null,
                'response': estados
            });
        }
    });

}

exports.SearchEstados = function(req, res, next) {

    let estados = req.body.estado;

    if (estados == undefined) {
        return res.status(406).json({
            'error': 'Invalid params!',
            'response': null
        });
    } else {
        Estado.findOne({ nome: estado }, function(err, estados) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
    
                return res.status(200).json({
                    'error': null,
                    'response': estados
                });
    
            }
        });
    }

}

