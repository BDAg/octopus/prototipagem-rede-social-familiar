const jwt = require('jsonwebtoken');
const ObjectId = require('mongoose').Types.ObjectId;

const Familia = require('../models/familiaM');
const secret = require('../_helpers/config');

exports.createFamily = function(req, res, next) {

    let nome = req.body.nome;
    let foto = req.body.foto;
    let descricao = req.body.descricao;
    let token = req.body.token;
    let privacidade = req.body.privacidade;

    let condition = nome == undefined || token == undefined;

    if (condition) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        let idUser = tokenDecoded['idUser'];

        let dados = {
            'nome': nome,
            'membros': [ ObjectId(idUser) ],
            'admins': [ ObjectId(idUser) ]
        };

        if (foto != undefined) {
            dados['foto'] = foto;
        }

        if (descricao != undefined) {
            dados['descricao'] = descricao;
        }

        if (privacidade != undefined) {
            dados['privacidade'] = privacidade;
        }

        let novaFamilia = new Familia(dados);

        novaFamilia.save(function(err, family) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                let infoToken = {
                    idFamily: family._id,
                    nome: family.nome
                }

                let token = jwt.sign(
                    infoToken,
                    secret.secret,
                    { expiresIn: "2h" }
                );

                return res.status(201).json({
                    'error': null,
                    'response': {
                        'message': 'Success!',
                        'data': family,
                        'token': token
                    }
                });
            }
        });
    }
}

exports.addMember = function(req, res, next) {

    let member = req.body.member;
    let idFamily = req.body.idFamily;
    let token = req.body.token;

    if(member == undefined || idFamily == undefined || token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        Familia.findById({ _id: idFamily }, (err, family) => {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                let admins = Array.from(family.admins);

                let permission = false;

                for (let value in admins) {
                    if (admins[value] == tokenDecoded['idUser']) {
                        permission = true;
                        break
                    }
                }

                if (permission) {
                    Familia.updateOne(
                        { _id: ObjectId(idFamily) },
                        {
                            $addToSet: { membros: ObjectId(member) }
                        },
                        function(err, result) {
                            if (err) {
                                return res.status(406).json({
                                    'response': null,
                                    'error': err
                                });
                            } else {
                                return res.status(200).json({
                                    'error': null,
                                    'response': {
                                        'message': 'success',
                                        'data': result
                                    }
                                });
                            }
                        }
                    );
                } else {
                    return res.status(401).json({
                        'response': null,
                        'error': 'Not allowed'
                    });
                }
            }
        });
    }
}

exports.joinFamily = function(req, res, next) {

    let token = req.body.token;
    let idFamily = req.body.idFamily;

    if (token == undefined || idFamily == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        Familia.updateOne(
            { _id: ObjectId(idFamily) },
            {
                $addToSet: { membros: ObjectId(tokenDecoded['idUser']) }
            },
            function(err, result) {
                if (err) {
                    return res.status(406).json({
                        'response': null,
                        'error': err
                    });
                } else {
                    return res.status(200).json({
                        'error': null,
                        'response': {
                            'message': 'success',
                            'data': result
                        }
                    });
                }
            }
        );
    }
}

exports.findFamily = function(req, res, next) {

    let token = req.body.token;

    if (token == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        let idUser = tokenDecoded['idUser'];

        Familia.find({ membros : { $in: [ ObjectId(idUser) ] } }, function(err, families) {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                return res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'Success!',
                        'data': families
                    }
                });

            }
        });

    }

}

exports.updateFotoPerfil = function(req, res, next) {

    let token = req.body.token;
    let idFamily = req.body.idFamily;
    let path = req.body.path;

    if (token == undefined || path == undefined || idFamily == undefined) {
        return res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {

        let tokenDecoded = jwt.verify(token, secret.secret);

        Familia.findById({ _id: idFamily }, (err, family) => {
            if (err) {
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {

                let admins = Array.from(family.admins);

                let permission = false;

                for (let value in admins) {
                    if (admins[value] == tokenDecoded['idUser']) {
                        permission = true;
                        break
                    }
                }

                if (permission) {
                    Familia.updateOne({ _id: idFamily }, { $set : { foto: path } }, function(err, result) {
                        if (err) {
                            return res.status(406).json({
                                'response': null,
                                'error': err
                            });
                        } else {
                            return res.status(200).json({
                                'error': null,
                                'response': {
                                    'message': 'Success!',
                                    'data': result
                                }
                            });
                        }
                    });
                } else {
                    return res.status(401).json({
                        'response': null,
                        'error': 'Not allowed'
                    });
                }
                
            }
        });

    }
    
}