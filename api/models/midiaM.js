const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectId = mongoose.Types.ObjectId;

const MidiaSchema = new Schema({
    tipo: { type: Number, required: 'Tipo é obrigatório' },
    legenda: { type: String, default: '' },
    checkin: { type: Array },
    dateTime: { type: Date, default: Date.now },
    idUsuario: { type: ObjectId, required: 'idUsuario é obrigatório' },
    idFamilia: { type: Array, required: 'idFamilia é obrigatório' },
    path: { type: String, required: 'path é obrigatrio'},
    like: { type: Array },
    comment: { type: Number, default: 0 },
    height: { type: Number, required: 'height é obrigatório' },
    width: { type: Number, required: 'width é obrigatório' },
});

// Tipos:
// 0 - Esporte
// 1 - Refeição
// 2 - Cultura
// 3 - Ar livre
// 4 - Festa
// 5 - Conquista

module.exports = mongoose.model('Midia', MidiaSchema);