const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectId = mongoose.Types.ObjectId;

const EstadoSchema = new Schema({
    nome: { type: String, required: 'Estado é obrigatório' },
    idPais: { type: ObjectId, required: 'idPais é obrigatório' },
});

module.exports = mongoose.model('Estado', EstadoSchema);