const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FamiliaSchema = new Schema({
    nome: { type: String, required: 'Nome é obrigatório' },
    foto: { type: String, default: '' },
    descricao: { type: String, default: '' },
    admins: { type: Array, required: 'Admin é obrigatório' },
    membros: { type: Array, required: 'Membros é obrigatório' },
    privacidade: { type: Number, default: 0 },
});

// 0 Público
// 1 Privado

module.exports = mongoose.model('Familia', FamiliaSchema);