const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PaisSchema = new Schema({
    nome: { type: String, required: 'Pais é obrigatório' }
});

module.exports = mongoose.model('Pais', PaisSchema);