const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectId = mongoose.Types.ObjectId;

const InteracoesSchema = new Schema({
    tipo: { type: Number, required: ' Tipo é obrigatório' },
    dateTime: { type: Date, default: Date.now },
    idMidia: { type: ObjectId, required: 'idMidia é obrigatório' },
    idUsuario: { type: ObjectId, required: 'idUsuario é obrigatório' },
});


module.exports = mongoose.model('Interações', InteracoesSchema);