const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectId = mongoose.Types.ObjectId;

const EmocaoSchema = new Schema({
    emocao: { type: String, required:'Emoção é obrigatoria' },
    datetime : {type: Date, default: Date.now },
    idUsuario : {type: ObjectId , required: 'idUsuario é obrigatorio'},
});

module.exports = mongoose.model('Emocao', EmocaoSchema);