const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectId = mongoose.Types.ObjectId;

const UserSchema = new Schema({
  nome: { type: String, minlength: 3, maxlength: 100, required: 'Nome é obrigatório' },
  sobrenome: { type: String, minlength: 3, maxlength: 100, required: 'Sobrenome é obrigatório' },
  dataNascimento: { type: Date, required: 'Data de Nascimento é obrigatório' },
  username: { type: String, minlength: 3, maxlength: 30, required: 'Username é obrigatório', index: { unique: true } },
  profissao: { type: String, default: '' },
  email: { type: String, minlength: 4, required: 'E-mail é obrigatório', index: { unique: true } },
  senha: { type: String, minlength: 6, required: 'Senha é obrigatório' },
  telefone: { type: String, minlength: 6, required: 'Telefone é obrigatório' },
  genero: { type: String, required: 'Gênero é obrigatório' },
  cidade: { type: ObjectId, required: 'Cidade é obrigatório' },
  criadoEm: { type: Date, default: Date.now },
  religiao: { type: String, maxlength: 50 },
  fotoPerfil: { type: String, default: '' },
  descricao: { type: String, default: '' },
  role: { type: String },
  codeSecurity: { type: String, default: '' }
});

module.exports = mongoose.model("Users", UserSchema);
