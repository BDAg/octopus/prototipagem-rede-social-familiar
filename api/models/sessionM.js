const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SessionSchema = new Schema({
    email: { type: String, minlength: 10, maxlength: 100, required: 'Required' },
    token: { type: String, required: 'Required' },
    criadoEm: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Session', SessionSchema);
