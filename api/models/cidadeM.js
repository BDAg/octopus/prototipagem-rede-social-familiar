const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ObjectId = mongoose.Types.ObjectId;

const CidadeSchema = new Schema({
    nome : {type: String , required : 'Nome é obrigatorio'},
    idEstado : {type: ObjectId , required: 'idEstado é obrigatorio'},
});

module.exports = mongoose.model('Cidade', CidadeSchema);